# front-restaurant

Application Web de gestion d'un restaurant.
L'application permet de prendre une réservation, et une partie administrareur permet
de gérer les tables, les horaires de service et les réservations.

Voir la [partie lancement](#lancement-du-projet) pour le démarrage du projet.

## Enonce

Application des gestion de reservations pour restaurants (3 max par groupe)
Contexte:

L'application demandée doit permettre à un restaurateur de gérer ses réservations via l'application.
Il doit avoir une vue globale lui permettant de piloter efficacement et simplement tout ce système.
L'ensemble doit être automatique et paramettrable, avec possibilité pour les clients de réserver via un formulaire.

Foctionnalités attendues:

Front:

    Formulaire de réservation (nom, prénom, email, heure, nombre de personnes, ... tout ce que vous jugez important)
    Administation avec les points ci-dessous.

Back:

    Gestion de comptes utilisateurs (interface pour se connecter à l'administration de l'application)
    Gestion des tables du restaurant
        ex: j'ai 10 tables, numérotées de 10 à 20. Je dois pouvoir les paramétrer dans l'application avec leur numérotation pour les identifier rapidement dans les différents menus
            je peux fermer les réservations pour une table spécifique. Imaginons que mes tables 9 et 10 soient à l'extérieur et qu'il pleut, il faut les fermer à la réservation.
    Gestion des heures de réservations
        je veux servir de 11h30 à 14h puis de 18h à 22h30 par exemple. Tout est répercuté sur le formulaire de reservation en front. 
            Il faut pouvoir définir différentes plages horaires selon le jour de la semaine. Par exemple pas de réservation possible le mardi, dimanche seulement un service au soir de 18h à 22h30
            Faire attention aux jours fériés / vacances du restaurant. Il faut quelque chose pour fermer les réservations à une date/heure.
    Gestion des réservations:
        récupérer les infos depuis le formulaire en front
            si besoin, je dois aussi pouvoir créer des réservations à la volée depuis l'admin pour une réservation par téléphone
            avoir un moyen simple de recontacter la personne qui a réservé (envoyer un e-mail ou SMS si vous voulez)
            pouvoir annuler / éditer une réservation (ex la personne n'est pas présente)
            attribuer une / des table(s) à une réservation
    Vue globale des tables (pourquoi pas avec un plan, ou juste un tableau)
        Voir si elle est réservée ou non
            Au clic pouvoir fermer/libérer la réservation pour la table en question ?

Attention pour ces points:

    Il faut calculer le nombre de tables réservées avant d'afficher les horaires disponibles en front. En effet, si j'ai déjà 10 réservations à 12h00 je ne dois plus pouvoir en faire de nouvelles. Pareil pour le nombre de personnes sélectionnable dans le formulaire (calculs à faire)
    Définir la durée d'une réservation, à vous de voir (1h ? 1h30 ?)

Pour la technique:

Il faut avoir un back et un front. Pour le back choisissez ce que vous voulez.
Si possible, tout mettre sur un dépôt avec les instructions pour installer le projet (pour que je puisse faire la correction une fois les projets terminés)

Concentrez-vous sur les fonctionnalités cette fois-ci. La présentation n'est pas le premier objectif. Si vous avez du temps faites-vous plaisir.

### Pages imaginées pour répondre à l'énoncé

- Page d'accueil (/accueil)
- Formulaire de réservation (nom, prénom, email, heure, nombre de personnes) (/reserv )
 - Tu peux accéder à la meme page de reserv en cliquant depuis les menus d'admin mais plutot que créer une réserv, tu l'edit (preremplir les champs, et changer l'appel de fin)
    ->  Enregistrer une reserv (nom, prenom, email, tel, nb_personnes, date) 
        -> test disponibilité effectué
        ->  mettre sur plusieurs tables si nécessaires
    ->  GetHoraire (Date)
    ->  DeleteReserv (avec l'id) (si edit passe pas, rollback transaction) 
- AdminPage des tables : 
  - Vue globale des tables(/admin)
    -> passageTrue/false des tables
    -> Obtenir les tables avec leur réservation
  - Gestion des heures de réservations (/admin) 
    -> modifier_horaire_journée (date_debut, date_fin, mode)
    -> rajouter_mode (nom_periode, debut_midi, fin_midi, debut_soir, fin_soir) 
    -> get_mode 
  - Affichage des reservs avec filtres (admin/reserv)
    -> delete une reserv (avec id) 
- Connexion (/login)
  - login 

## Technologies utilisées

La partie Front est faîtes en typescript avec React.js et des composants
tirés de [Material-UI](https://mui.com/material-ui/getting-started/).
  La partie Backend est faîtes avec Java Spring.
La base de donnée est gérée avec PostgreSQL. Un docker-compose est fourni pour l'initialiser.

### Tables de la BDD

Table user

| login   |   pwd  |
| ------- | ------ |

Table réservation

| nom/prenom  | date | email | tel | nb_personnes |
| ----------- | -----| ----- | --- | ------------ |

Table paramétrage de réservation

| duree_reservation |
| ----------------- |

Table de jointure entre les réservations et les tables

| id_reserv   |   id_table |
| ----------- | ---------- |

Table des tables

| num_table   |   dispo   |   nb_personnes |
| ----------- | --------- | -------------- |

Table de parametrage des heures de service

| periode (Lundi...Dimanche, Vacance, Férié, Custom)   |   debut_midi   |   fin_midi   |   debut_soir   |   fin_soir  |
| ---------------------------------------------------- | -------------- | ------------ | -------------- | ----------- |

Table des heures de service

| date_debut | date_fin   |  mode   |
| ---------- | ---------- | ------- |

## Lancement du projet

#### Lancement du back

Dans le dossier backend, un docker-compose est fourni pour créer la base de donnée sur un container PostgreSQL.
```
cd backend
docker-compose up
```
Une fois la bdd opérationelle, il suffit de lancer le projet JavaSpring avec maven.
```
cd backend
mvn clean install
mvn spring-boot:run
```

#### Lancement du front

Dans le dossier frontend, il suffit de lancer le projet React.js avec npm.
```
cd frontend
npm install
npm start
```

### Parcours Type

L'utilisateur arrive sur la page d'accueil (localhost:3000/), et accède directement à la page de réservation.
  En remplissant les différents champs, il peut ensuite valider sa réservation (les champs sont conservés après validation
pour simplifier les tests, et permettre de prendre rapidement plusieurs réservations).

Une page d'administration est accessible via le menu, mais demande un login/mot de passe pour y accéder. L'utilisateur "admin"
avec le mot de passe "admin" permet de se connecter.  
Une fois connecté, l'administrateur peut voir les différentes tables, et au clic sur une table, les réservations de cette table
ainsi que la possibilité de désactiver les réservations sur cette table.  
Il peut aussi voir la liste de toutes les réservations. Au clic sur une réservation, l'administrateur est redirigé
vers une page permettant d'éditer la réservation. Au clic sur le bouton "Supprimer", la réservation est supprimée.

L'administrarteur peut aussi accéder au menu gestion d'horaire, il peut sélectionner un type d'horaire à appliquer exceptionnellement sur une période,
ou bien créer un nouveau mode d'horaire applicable.

