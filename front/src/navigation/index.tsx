import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import { useAuthContext } from '../features/auth/context/AuthProvider';
import { ProtectedRoute } from './components/ProtectedRoute';
import ReservationPage from '../features/reservation/page/ReservationPage';
import AdminPage from '../features/admin/page/AdminPage';
import Logout from '../features/auth/page/Logout';
import RootLayout from '../layouts/RootLayout';
import Login from '../features/auth/page/Login';

const Routes = () => {
	const token = useAuthContext()?.state.token;

	const routesForPublic = [
		{
			path: '/',
			element: <ReservationPage />,
		},
		{
			path: '/login',
			element: <Login />,
		},
	];

	const routesForAuthenticatedOnly = [
		{
			path: '/',
			element: <ProtectedRoute />,
			children: [
				{
					path: '/admin',
					element: <AdminPage />,
				},
				{
					path: '/logout',
					element: <Logout />,
				},
			],
		},
	];

	const router = createBrowserRouter([
		{
			path: '/',
			element: <RootLayout />,
			children: [...routesForPublic, ...routesForAuthenticatedOnly],
		},
	]);

	return <RouterProvider router={router} />;
};

export default Routes;
