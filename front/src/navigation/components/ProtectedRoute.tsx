import { Navigate, Outlet } from 'react-router-dom';
import { useAuthContext } from '../../features/auth/context/AuthProvider';

export const ProtectedRoute = () => {
	const token = useAuthContext()?.state.token;
	if (!token) return <Navigate to="/login" />;

	return <Outlet />;
};
