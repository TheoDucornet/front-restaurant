import axios, {AxiosInstance, CancelToken} from 'axios';
import Cookies from "universal-cookie";

interface HttpConfig {
    method: string;
    url: string;
    headers: any;
    cancelToken: CancelToken;
    data?: any;
}

export default class HttpService {
    _baseUrl: string;
    _instance: AxiosInstance;

    constructor(baseUrl = 'http://127.0.0.1:8080') {
        this._baseUrl = baseUrl;
        this._instance = axios.create({baseURL: this._baseUrl});
    }

    get defaultHeaders() {
        const token = new Cookies().get('token');
        return token ? {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json',
        } : {
            'Content-Type': 'application/json',
        };
    }

    basicAuthHeaders(username: string, pwd: string) {
        return {
            'Authorization': `Basic ${btoa(`${username}:${pwd}`)}`,
            'Content-Type': 'application/json',
        };
    }

    set_baseUrl(newUrl: string) {
        this._baseUrl = newUrl;
        this._instance.defaults.baseURL = newUrl;
    }

    private async loginRequest(
        username: string,
        pwd: string
    ) {
        const headers = this.basicAuthHeaders(username, pwd);
        const source = axios.CancelToken.source();

        const config: HttpConfig = {
            method: 'POST',
            url: '/login',
            headers,
            cancelToken: source.token,
        };

        return {
            request: this._instance(config),
            cancel: source.cancel,
        };
    }

    private async request(
        method: string,
        url: string,
        data = null,
        customHeaders = {}
    ) {
        const headers = {...this.defaultHeaders, ...customHeaders};
        const source = axios.CancelToken.source();

        const config: HttpConfig = {
            method,
            url,
            headers,
            cancelToken: source.token,
        };

        if (data) config.data = data;

        return {
            request: this._instance(config),
            cancel: source.cancel,
        };
    }

    login(username: string, pwd: string) {
        return this.loginRequest(username, pwd);
    }

    get(url: string, customHeaders = {}) {
        return this.request('GET', url, null, customHeaders);
    }

    post(url: string, data: any, customHeaders = {}) {
        return this.request('POST', url, data, customHeaders);
    }

    put(url: string, data: any, customHeaders = {}) {
        return this.request('PUT', url, data, customHeaders);
    }

    delete(url: string, customHeaders = {}) {
        return this.request('DELETE', url, null, customHeaders);
    }
}
