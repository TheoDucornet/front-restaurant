import {Outlet} from "react-router-dom";
import {Container} from "@mui/material";
import NavMenus from "../common/components/NavMenus";

const RootLayout = () => {
    return (
        <>
            <NavMenus/>
            <Container>
                <main>
                    <Outlet/>
                </main>
            </Container>
        </>
    )
}

export default RootLayout;