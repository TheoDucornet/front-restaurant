import { AppBar, Box, Button, Toolbar, Typography } from '@mui/material';
import RestaurantIcon from '@mui/icons-material/Restaurant';

const NavMenus = () => (
	<Box sx={{ flexGrow: 1 }}>
		<AppBar position="static">
			<Toolbar>
				<Box
					sx={{
						p: 2,
						border: '1px solid black',
						display: 'flex',
					}}
				>
					<RestaurantIcon />
					<Typography
						variant="h6"
						noWrap
						component="a"
						sx={{
							ml: 2,
							mr: 2,
							display: { xs: 'none', md: 'flex' },
							fontFamily: 'roboto',
							fontWeight: 900,
							letterSpacing: '.3rem',
							color: 'inherit',
							textDecoration: 'none',
						}}
						href={'/'}
					>
						RESTAURANT
					</Typography>
				</Box>
				<Typography
					variant="h6"
					component="a"
					sx={{
						ml: 15,
						mr: 10,
						display: { xs: 'none', md: 'flex' },
						fontFamily: 'monospace',
						fontWeight: 700,
						color: 'inherit',
						textDecoration: 'none',
					}}
					href={'/'}
				>
					Réservations
				</Typography>
				<Typography
					variant="h6"
					component="a"
					sx={{
						flexGrow: 1,
						display: { xs: 'none', md: 'flex' },
						fontFamily: 'monospace',
						fontWeight: 700,
						color: 'inherit',
						textDecoration: 'none',
					}}
					href={'/admin'}
				>
					Gestion
				</Typography>
				<Box
					sx={{
						border: '1px solid grey',
						borderRadius: '20px',
						bgcolor: '#1769aa',
					}}
				>
					<Button
						color="inherit"
						sx={{ fontWeight: 700 }}
						href={'/login'}
					>
						Connexion
					</Button>
				</Box>
			</Toolbar>
		</AppBar>
	</Box>
);

export default NavMenus;
