import React from 'react';
import ReactDOM from 'react-dom/client';
import AuthProvider from './features/auth/context/AuthProvider';
import Routes from './navigation';

const root = ReactDOM.createRoot(
	document.getElementById('root') as HTMLElement
);

root.render(
	<AuthProvider>
		<Routes />
	</AuthProvider>
);
