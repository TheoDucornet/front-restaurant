import {Box} from "@mui/material";
import React, {useState} from "react";
import dayjs, {Dayjs} from "dayjs";
import ReservationTitle from "./ReservationTitle";
import ReservationForm from "./ReservationForm";
import {ReservationStateType, ReservationViewType} from "../types/ReservationTypes";
import {ChangeReservationStateContext, ReservationStateContext} from "../context/ReservationContext";

const ReservationView = ({reservation, horaires, getHoraire, handleValidatedForm}: ReservationViewType) => {

    const [state, setState] = useState<ReservationStateType>({
        nom: reservation?.nom || "",
        prenom: reservation?.prenom || "",
        telephone: reservation?.tel || "",
        email: reservation?.email || "",
        dateReservation: reservation?.date ? dayjs(reservation?.date) : null,
        heureReservation: reservation?.date ? dayjs(reservation?.date) : null,
        nombrePersonnes: reservation?.nb_personnes,
        errorMessage: null,
    });
    const handleChange = (key: keyof ReservationStateType, value: string | Dayjs | number | null) => {
        console.log(value);
        if (key === "dateReservation" && dayjs.isDayjs(value)) {
            const formattedDate = value.format("YYYY-MM-DD");
            getHoraire(formattedDate);
        }
        setState({...state, [key]: value});
    };
    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        const {nom, prenom, telephone, email, dateReservation, heureReservation, nombrePersonnes} = state;
        e.preventDefault();
        if (nom.trim() === "" || prenom.trim() === "" || isNaN(Number(telephone)) || email.trim() === "" || heureReservation === null ||
            dateReservation === null || nombrePersonnes === 0 || isNaN(Number(nombrePersonnes))) {
            setState({...state, errorMessage: "Veuillez remplir tous les champs correctement."});
            return;
        }
        setState({...state, errorMessage: null});
        handleValidatedForm({
            id: reservation?.id || 0,
            nom: nom,
            prenom: prenom,
            email: email,
            tel: telephone,
            date: dateReservation ? dateReservation.add(1, 'day').toISOString() : '',
            nb_personnes: nombrePersonnes ? nombrePersonnes : 0
        });
    };

    return (
        <Box sx={{backgroundColor: '#f0f0f0', padding: '20px'}}>
            <ReservationTitle/>
            <ReservationStateContext.Provider value={state}>
                <ChangeReservationStateContext.Provider value={{handleChange}}>
                    <ReservationForm handleSubmit={handleSubmit} availableHoraire={horaires}/>
                </ChangeReservationStateContext.Provider>
            </ReservationStateContext.Provider>
        </Box>
    );
};

export default ReservationView;
