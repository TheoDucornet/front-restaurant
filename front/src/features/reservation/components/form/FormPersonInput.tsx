import {Box, Input, Typography} from "@mui/material";
import React, {useContext} from "react";
import {
    ChangeReservationStateContext,
    ReservationStateContext
} from "../../context/ReservationContext";
import {ReservationDico} from "../../dico/ReservationDico";

const FormPersonInput = () => {
    const {nom, prenom,telephone, email, errorMessage} = useContext(ReservationStateContext);
    const {handleChange} = useContext(ChangeReservationStateContext);

    return (
        <>
            <Box sx={{marginTop: '30px', marginBottom: '10px'}}>
                <Typography
                    variant="body1"
                    component="h5"
                    sx={{fontWeight: 700}}
                >
                    {ReservationDico.NOM}
                </Typography>
                <Input
                    placeholder={ReservationDico.NOM}
                    fullWidth
                    value={nom}
                    onChange={(e) => handleChange("nom", e.target.value)}
                    error={errorMessage !== null && nom.trim() === ""}
                />
            </Box>
            <Box sx={{marginTop: '30px', marginBottom: '10px'}}>
                <Typography
                    variant="body1"
                    component="h5"
                    sx={{fontWeight: 700}}
                >
                    {ReservationDico.PRENOM}
                </Typography>
                <Input
                    placeholder={ReservationDico.PRENOM}
                    fullWidth
                    value={prenom}
                    onChange={(e) => handleChange("prenom", e.target.value)}
                    error={errorMessage !== null && prenom.trim() === ""}
                />
            </Box>
            <Box sx={{marginTop: '30px', marginBottom: '10px'}}>
                <Typography
                    variant="body1"
                    component="h5"
                    sx={{fontWeight: 700}}
                >
                    {ReservationDico.TELEPHONE}
                </Typography>
                <Input
                    type="number"
                    placeholder={ReservationDico.TELEPHONE}
                    fullWidth
                    value={telephone}
                    onChange={(e) => handleChange("telephone", e.target.value)}
                    error={errorMessage !== null && telephone.trim() === ""}
                />
            </Box>
            <Box sx={{marginTop: '30px', marginBottom: '10px'}}>
                <Typography
                    variant="body1"
                    component="h5"
                    sx={{fontWeight: 700}}
                >
                    {ReservationDico.EMAIL}
                </Typography>
                <Input
                    placeholder={ReservationDico.EMAIL}
                    fullWidth
                    value={email}
                    onChange={(e) => handleChange("email", e.target.value)}
                    error={errorMessage !== null && email.trim() === ""}
                />
            </Box>
        </>
    );
};

export default FormPersonInput;
