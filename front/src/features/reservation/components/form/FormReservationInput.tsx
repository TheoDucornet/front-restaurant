import {Box, FormControlLabel, MenuItem, Radio, RadioGroup, Select, Typography} from "@mui/material";
import React, {useContext} from "react";
import {DatePicker, LocalizationProvider, TimePicker} from "@mui/x-date-pickers";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";
import {ChangeReservationStateContext, ReservationStateContext} from "../../context/ReservationContext";
import {ReservationDico} from "../../dico/ReservationDico";
import {AvailableHoraireType} from "../../types/ReservationTypes";
import dayjs from "dayjs";

type FormReservationInputProps = {
    availableHoraire: AvailableHoraireType | undefined;
};

const FormReservationInput = ({availableHoraire}: FormReservationInputProps) => {
    const {nombrePersonnes, dateReservation, heureReservation, errorMessage} = useContext(ReservationStateContext);
    const {handleChange} = useContext(ChangeReservationStateContext);

    const [selectedPeriod, setSelectedPeriod] = React.useState<string>('midi');

    const menuItems = Array.from({length: 10}, (_, i) => (
        <MenuItem key={i + 1} value={i + 1}>{i + 1}</MenuItem>
    ));

    return (
        <>
            <Box sx={{marginTop: '30px'}} display={'flex'}>
                <Typography
                    variant="body1"
                    component="h5"
                    sx={{fontWeight: 700}}
                >
                    {ReservationDico.NOMBRE_PERSONNES} : &nbsp;
                </Typography>
                <Select
                    value={nombrePersonnes}
                    onChange={(e) => handleChange("nombrePersonnes", e.target.value)}
                    error={errorMessage !== null && nombrePersonnes === undefined}
                >
                    {menuItems}
                </Select>
            </Box>
            <Box sx={{marginTop: '15px', marginBottom: '20px'}}>
                <RadioGroup row aria-label="periode"
                            name="periode"
                            value={selectedPeriod}
                            onChange={(e) => {
                                setSelectedPeriod(e.target.value);
                                handleChange("heureReservation", null);
                            }}
                >
                    <FormControlLabel value="midi" control={<Radio/>} label="Midi"/>
                    <FormControlLabel value="soir" control={<Radio/>} label="Soir"/>
                </RadioGroup>
            </Box>
            <Box sx={{marginTop: '15px', marginBottom: '20px'}}>
                <Typography
                    variant="body1"
                    component="h5"
                    sx={{fontWeight: 700, marginBottom: '10px'}}
                >
                    {ReservationDico.DATE_RESERVATION}
                </Typography>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DatePicker
                        label={ReservationDico.DATE_RESERVATION}
                        value={dateReservation}
                        onChange={(newValue) => {
                            handleChange("heureReservation", null);
                            handleChange("dateReservation", newValue);
                        }}
                        disablePast
                    />
                    {selectedPeriod === 'midi' ? (<TimePicker
                        label={ReservationDico.HEURE_RESERVATION}
                        value={heureReservation}
                        onChange={(newValue) => handleChange("heureReservation", newValue)}
                        disablePast
                        disabled={dateReservation === null}
                        minTime={availableHoraire ? dayjs(availableHoraire.debut_midi, 'HH:mm') : undefined}
                        maxTime={availableHoraire ? dayjs(availableHoraire.fin_midi, 'HH:mm') : undefined}
                    />) : (<TimePicker
                        label={ReservationDico.HEURE_RESERVATION}
                        value={heureReservation}
                        onChange={(newValue) => handleChange("heureReservation", newValue)}
                        disablePast
                        disabled={dateReservation === null}
                        minTime={availableHoraire ? dayjs(availableHoraire.debut_soir, 'HH:mm') : undefined}
                        maxTime={availableHoraire ? dayjs(availableHoraire.fin_soir, 'HH:mm') : undefined}
                    />)
                    }

                </LocalizationProvider>
            </Box>
        </>
    );
};

export default FormReservationInput;
