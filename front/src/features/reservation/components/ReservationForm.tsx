import {Button, Typography} from "@mui/material";
import React, {useContext} from "react";
import {ReservationStateContext} from "../context/ReservationContext";
import FormPersonInput from "./form/FormPersonInput";
import FormReservationInput from "./form/FormReservationInput";
import {AvailableHoraireType, ReservationFormProps} from "../types/ReservationTypes";
import {ReservationDico} from "../dico/ReservationDico";

const ReservationForm: React.FC<ReservationFormProps> = ({handleSubmit, availableHoraire}) => {
    const {errorMessage} = useContext(ReservationStateContext);

    return (
        <form onSubmit={handleSubmit}>
            {errorMessage && (
                <Typography variant="body2" color="error" sx={{textAlign: 'center', marginTop: '10px'}}>
                    {errorMessage}
                </Typography>
            )}
            <FormPersonInput/>
            <FormReservationInput availableHoraire={availableHoraire}/>
            <Button type="submit" variant="contained" color="primary">{ReservationDico.VALIDER_RESERV}</Button>
        </form>
    );
};

export default ReservationForm;
