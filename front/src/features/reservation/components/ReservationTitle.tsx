import {Typography} from "@mui/material";
import React from "react";
import {ReservationDico} from "../dico/ReservationDico";

const ReservationTitle = () => (
    <Typography
        variant="h4"
        component="h4"
        sx={{
            fontFamily: 'monospace',
            fontWeight: 700,
            textDecoration: 'none',
            marginTop: '20px',
            textAlign: 'center',
        }}
    >
        {ReservationDico.PAGE_TITLE}
    </Typography>
)

export default ReservationTitle;
