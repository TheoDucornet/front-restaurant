import ReservationView from "../components/ReservationView";
import HttpService from "../../../http/HttpService";
import {useLocation} from "react-router-dom";
import {AvailableHoraireType} from "../types/ReservationTypes";
import {useState} from "react";
import {ReservationDataFormTypes, ReservationDataTypes} from "../../admin/types/AdminTypes";

const ReservationPage = () => {
    const apiService = new HttpService();
    const {state} = useLocation();

    const [horaires, setHoraires] = useState<AvailableHoraireType | undefined>(undefined);

    const getHoraire = (date: string): void => {
        apiService.get("/reserv/horaire/" + date).then(({request}) => {
            request.then((response) => {
                console.log(response.data);
                setHoraires(response.data);
            })
                .catch((error) => {
                    alert(error);
                    console.error(error);
                });
        });
    };

    const handleValidatedForm = (reservation: ReservationDataFormTypes): void => {
        console.log(reservation);
        if(state?.reservation) {
            updateReservation(reservation);
        } else {
            insertReservation(reservation);
        }
    }

    const insertReservation = (reservation: ReservationDataFormTypes): void => {
        apiService.post("/reserv", reservation).then(({request}) => {
            request.then((response) => {
                console.log(response.data);
                alert("Réservation effectuée avec succès");
            })
                .catch((error) => {
                    alert(error);
                    console.error(error);
                });
        });
    }

    const updateReservation = (reservation: ReservationDataFormTypes): void => {
        console.log(reservation);
        apiService.put("/reserv/" + reservation.id, reservation).then(({request}) => {
            request.then((response) => {
                console.log(response.data);
                alert("Réservation modifiée avec succès");
            })
                .catch((error) => {
                    alert(error);
                    console.error(error);
                });
        });

    }

    return (
        <ReservationView
            reservation={state?.reservation}
            horaires={horaires}
            getHoraire={getHoraire}
            handleValidatedForm={handleValidatedForm}
        />
    );
};

export default ReservationPage;
