export enum ReservationDico {
    PAGE_TITLE = "Formulaire de réservation",
    VALIDER_RESERV = "Valider la réservation",
    NOM = "Nom",
    PRENOM = "Prénom",
    EMAIL = "E-mail",
    DATE_RESERVATION = "Date de réservation",
    HEURE_RESERVATION = "Heure de réservation",
    NOMBRE_PERSONNES = "Nombre de personnes",
    TELEPHONE = "Téléphone",
}