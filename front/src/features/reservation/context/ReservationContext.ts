import {createContext} from 'react';
import {ChangeReservationStateType, ReservationStateType} from "../types/ReservationTypes";

export const ReservationStateContext = createContext<ReservationStateType>({
    nom: "",
    prenom: "",
    telephone: "",
    email: "",
    dateReservation: null,
    heureReservation: null,
    nombrePersonnes: undefined,
    errorMessage: null,
});
export const ChangeReservationStateContext = createContext<ChangeReservationStateType>({
    handleChange: () => {
        throw new Error("La fonction handleChange n'est pas initialisée correctement.");
    }
});