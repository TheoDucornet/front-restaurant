import {Dayjs} from "dayjs";
import React from "react";
import {ReservationDataTypes} from "../../admin/types/AdminTypes";

export interface ReservationStateType {
    nom: string;
    prenom: string;
    email: string;
    telephone: string;
    dateReservation: Dayjs | null | undefined;
    heureReservation: Dayjs | null | undefined;
    nombrePersonnes: number | undefined;
    errorMessage: string | null;
}

export interface ChangeReservationStateType {
    handleChange: (key: keyof ReservationStateType, value: string | Dayjs |number | null) => void;
}

export interface ReservationFormProps {
    handleSubmit: (e: React.FormEvent<HTMLFormElement>) => void;
    availableHoraire : AvailableHoraireType | undefined;
}

export interface ReservationViewType {
    reservation: ReceivedReservationFieldsType | undefined;
    horaires : AvailableHoraireType | undefined;
    getHoraire : (date: string) => void;
    handleValidatedForm : (reservation: ReservationDataTypes) => void;
}

export interface ReceivedReservationFieldsType {
    id? : number;
    nom: string;
    prenom: string;
    email: string;
    tel: string;
    date: Dayjs | null | undefined;
    nb_personnes: number | undefined;
}

export interface  AvailableHoraireType {
    periode: string,
    debut_midi: string,
    fin_midi: string,
    debut_soir: string,
    fin_soir: string
}