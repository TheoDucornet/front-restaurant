import React from "react";
import {Order} from "../utils/TableUtils";
import {Dayjs} from "dayjs";

export interface ReservationDataTypes {
    id: number;
    date: string;
    email: string;
    tel: string;
    prenom: string;
    nom: string;
    nb_personnes: number;
}

export interface ReservationDataFormTypes {
    id?: number;
    date: string;
    email: string;
    tel: string;
    prenom: string;
    nom: string;
    nb_personnes: number;
}

export interface ReservationsDataTypes {
    reservations: ReservationDataTypes[]
}

export interface TableDataType {
    numTable: number,
    dispo: boolean,
    nb_places: number,
    reservs: ReservationDataTypes[]
}

export interface HorairesTypes {
    periode: string,
    debut_midi: string,
    fin_midi: string,
    debut_soir: string,
    fin_soir: string,
}

export interface NewHoraireModeDataTypes {
    periode: string,
    debut_midi: string,
    fin_midi: string,
    debut_soir: string,
    fin_soir: string,
}

export interface NewHoraireServiceDataTypes {
    "debut_midi": string,
    "fin_midi": string,
    "mode": string
}

export interface TablesDataTypes {
    tables: TableDataType[],
    deleteReserv: (id: number) => void,
    updateDispoTable: (id: number) => void,
}

export interface AdminDataTypes {
    horaires: HorairesTypes[],
    tables: TableDataType[],
    deleteReserv: (id: number) => void,
    updateDispoTable: (id: number) => void,
    addHoraires: (newHoraire: NewHoraireModeDataTypes) => void,
    addHoraireService: (newHoraire: NewHoraireServiceDataTypes) => void,
}

export interface GestionHoraireTypes {
    horaires: HorairesTypes[],
    addHoraires: (newHoraire: NewHoraireModeDataTypes) => void,
    addHoraireService: (newHoraire: NewHoraireServiceDataTypes) => void,
}

export interface AdminTableButtonTypes {
    numTable: number,
    setIsModalOpen: (arg0: boolean) => void,
    changeTableModalIndex: (arg0: number) => void,
}

export interface ModalModifTablesTypes {
    table: TableDataType,
    setIsModalOpen: (arg0: boolean) => void,
    isModalOpen: boolean,
    updateDispoTable: (id: number) => void,
    deleteReserv: (id: number) => void,
}

export interface ColumnTableReservationTypes {
    id: keyof ReservationDataTypes;
    label: string;
    numeric: boolean;
}

export interface EnhancedTableProps {
    onRequestSort: (event: React.MouseEvent<unknown>, property: keyof ReservationDataTypes) => void;
    order: Order;
    orderBy: string;
}

export interface TableauReservationFiltersProps extends ReservationsDataTypes {
    setDisplayedReservations: React.Dispatch<React.SetStateAction<ReservationDataTypes[]>>;
}