import {ColumnTableReservationTypes} from "../types/AdminTypes";

const TableauReservationColumns: readonly ColumnTableReservationTypes[] = [
    {id: 'date', label: 'Date', numeric : false},
    {id: 'nom', label: 'Nom', numeric : false},
    {id: 'prenom', label: 'Prénom', numeric : false},
    {id: 'nb_personnes', label: 'Nombre', numeric : false},
    {id: 'tel', label: 'Téléphone', numeric : false},
    {id: 'email', label: 'Mail', numeric : false},
];

export default TableauReservationColumns;

