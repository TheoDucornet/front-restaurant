import {Box, Button, Dialog, DialogActions, DialogContent, Switch, Typography} from "@mui/material";
import React, {useState} from "react";
import {ModalModifTablesTypes} from "../../types/AdminTypes";
import {AdminDico} from "../../dico/AdminDico";
import TableauReservations from "./TableauReservations";

const ModalModifTable = ({table, setIsModalOpen, isModalOpen, updateDispoTable, deleteReserv}: ModalModifTablesTypes) => {
    const closeModal = () => {
        setIsModalOpen(false);
    };

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        updateDispoTable(table.numTable);
        closeModal();
    };

    return (
        <>
            <Dialog open={isModalOpen} onClose={closeModal}>
                <DialogContent>
                    <Typography
                        variant="h4"
                        component="h4"
                    >
                        {AdminDico.DETAIL_TABLE} {table.numTable}
                    </Typography>
                    <Box sx={{marginTop: '30px'}} display={'flex'}>
                        <Typography
                            variant="h6"
                            component="h6"
                        >
                            {AdminDico.POSSIBILITE_RESERVATION}
                        </Typography>
                        <Switch
                            checked={table.dispo}
                            onChange={handleChange}
                        />
                    </Box>
                    <Typography
                        variant="h6"
                        component="h6"
                        sx={{marginTop: '30px'}}>
                        {AdminDico.RESERVATIONS_EN_COURS}
                    </Typography>
                    <TableauReservations reservations={table.reservs} deleteReserv={deleteReserv}/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={closeModal}>Fermer</Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default ModalModifTable;
