import {Typography} from "@mui/material";
import {AdminDico} from "../../dico/AdminDico";
import TableauReservations from "./TableauReservations";
import React, {useEffect} from "react";
import {TableDataType, TablesDataTypes} from "../../types/AdminTypes";
import TableauReservationFilters from "./TableauReservationFilters";

type GestionReservationProps = {
    tables : TableDataType[],
    deleteReserv: (id: number) => void,
}
const GestionReservation = ({tables, deleteReserv}: GestionReservationProps) => {

    const reservations = tables.map((table) => {
        return table.reservs;
    }).flat()

    const [displayedReservations, setDisplayedReservations] = React.useState(
        reservations
    );

    useEffect(() => {
        setDisplayedReservations(reservations);
    }, [tables]);

    return (
        <>
            <Typography
                variant="h4"
                component="h4"
                sx={{
                    fontFamily: 'monospace',
                    fontWeight: 700,
                    textDecoration: 'none',
                    marginTop: '40px',
                    marginBottom: '5px',
                    textAlign: 'center',
                }}
            >
                {AdminDico.RESERVATIONS}
            </Typography>
            <TableauReservations reservations={displayedReservations} deleteReserv={deleteReserv}/>
            <TableauReservationFilters reservations={reservations} setDisplayedReservations={setDisplayedReservations}/>
        </>
    )
}

export default GestionReservation;