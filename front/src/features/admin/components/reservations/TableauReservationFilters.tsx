import React from "react";
import {Typography, Box, TextField} from "@mui/material";
import {DatePicker, LocalizationProvider} from "@mui/x-date-pickers";
import {ReservationDataTypes, TableauReservationFiltersProps} from "../../types/AdminTypes";
import {Dayjs} from "dayjs";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";

const TableauReservationFilters = ({reservations, setDisplayedReservations}: TableauReservationFiltersProps) => {
    const [selectedDate, setSelectedDate] = React.useState<Dayjs | null>(null);

    const handleDateChange = (date: Dayjs | null) => {
        setSelectedDate(date);
        filterReservations({date: date?.toISOString().split('T')[0], nom: '', prenom: ''});
    };

    const handleFilterChange = (filterKey: keyof ReservationDataTypes, value: string) => {
        filterReservations({date: selectedDate ? selectedDate.toISOString() : undefined, [filterKey]: value});
    };

    const compareDates = (date1: string, date2: string): boolean => {
        const d1 = new Date(date1);
        const d2 = new Date(date2);

        d1.setHours(0,0,0,0);
        d2.setHours(0,0,0,0);

        console.log(date1);
        console.log(date2);

        const year1 = d1.getFullYear();
        const month1 = d1.getMonth();
        const day1 = d1.getDate();

        const year2 = d2.getFullYear();
        const month2 = d2.getMonth();
        const day2 = d2.getDate()+1;

        console.log(year1, month1, day1, year2, month2, day2);
        console.log(year1 === year2, month1 === month2, day1 === day2);

        return year1 === year2 && month1 === month2 && day1 === day2;
    }

    const filterReservations = (filters: Partial<ReservationDataTypes>) => {
        const filteredReservations = reservations.filter(reservation => {
            return Object.entries(filters).every(([key, value]) => {
                if (key === 'date') {
                    if (value === undefined) return true;
                    return compareDates(reservation.date, value.toString());
                } else {
                    if (value === undefined) return true;
                    const reservationValue = reservation[key as keyof ReservationDataTypes];
                    const filterValue = typeof value === 'string' ? value.toLowerCase() : value?.toString().toLowerCase();
                    return reservationValue !== undefined && reservationValue.toString().toLowerCase().includes(filterValue as string);
                }
            });
        });
        setDisplayedReservations(filteredReservations);
    };

    return (
        <Box sx={{p: 2}}>
            <Typography variant="h6" gutterBottom>
                Filtrer les réservations
            </Typography>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DatePicker
                    label="Date"
                    value={selectedDate}
                    onChange={(date) => handleDateChange(date)}
                    sx={{mr: 2, mb: 2}}
                />
            </LocalizationProvider>
            <TextField
                label="Nom"
                variant="outlined"
                onChange={(e) => handleFilterChange('nom', e.target.value)}
                sx={{mr: 2, mb: 2}}
            />
            <TextField
                label="Prénom"
                variant="outlined"
                onChange={(e) => handleFilterChange('prenom', e.target.value)}
                sx={{mb: 2}}
            />
        </Box>
    );
}

export default TableauReservationFilters;