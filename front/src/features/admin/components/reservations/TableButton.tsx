import {Button} from "@mui/material";
import React from "react";
import {AdminTableButtonTypes} from "../../types/AdminTypes";
import TableRestaurantOutlinedIcon from '@mui/icons-material/TableRestaurantOutlined';

const TableButton = ({numTable, setIsModalOpen, changeTableModalIndex}: AdminTableButtonTypes) => {
    const openModal = () => {
        changeTableModalIndex(numTable);
        setIsModalOpen(true);
    };

    return (
        <>
            <Button
                variant="outlined"
                onClick={openModal}
            >
                <TableRestaurantOutlinedIcon/>
                Table {numTable}
            </Button>
        </>
    );
};

export default TableButton;
