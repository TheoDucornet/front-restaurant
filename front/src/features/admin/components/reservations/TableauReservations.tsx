import {Button, Table, TableBody, TableCell, TableContainer, TablePagination, TableRow} from "@mui/material";
import React, {useState} from "react";
import {getComparator, Order, stableSort} from "../../utils/TableUtils";
import {ReservationDataTypes, ReservationsDataTypes} from "../../types/AdminTypes";
import TableauReservationHeader from "./TableauReservationHeader";
import TableauReservationColumns from "../../const/TableauReservationColumns";
import {useNavigate} from "react-router-dom";

type TableauReservationsProps = {
    reservations : ReservationDataTypes[],
    deleteReserv: (id: number) => void,
}

const TableauReservations = ({reservations, deleteReserv }: TableauReservationsProps) => {

    const navigate = useNavigate();

    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [order, setOrder] = React.useState<Order>('asc');
    const [orderBy, setOrderBy] = React.useState<keyof ReservationDataTypes>('date');

    const handleRequestSort = (
        event: React.MouseEvent<unknown>,
        property: keyof ReservationDataTypes,
    ) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    const visibleRows = React.useMemo(
        () =>
            stableSort(reservations, getComparator(order, orderBy)).slice(
                page * rowsPerPage,
                page * rowsPerPage + rowsPerPage,
            ),
        [reservations, order, orderBy, page, rowsPerPage],
    );

    return (
        <>
            <TableContainer sx={{maxHeight: 440}}>
                <Table stickyHeader aria-label="sticky table">
                    <TableauReservationHeader
                        order={order}
                        orderBy={orderBy}
                        onRequestSort={handleRequestSort}
                    />
                    <TableBody>
                        {visibleRows
                            .map((reservation, index) => {
                                return (
                                    <TableRow hover role="checkbox" tabIndex={-1} key={index} onClick={(e) => {
                                        e.preventDefault();
                                        navigate('/', {
                                            state: {
                                                reservation : reservation,
                                            }
                                        });
                                    }}>
                                        {TableauReservationColumns.map((column) => {
                                            const value = reservation[column.id];
                                            return (
                                                <TableCell key={column.id}>
                                                    {value}
                                                </TableCell>
                                            );
                                        })}
                                        <TableCell>
                                            <Button color={"error"} onClick={(e) => {
                                                e.preventDefault();
                                                e.stopPropagation();
                                                deleteReserv(reservation.id);
                                            }}>
                                                Supprimmer
                                            </Button>
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[2, 5, 10]}
                component="div"
                count={reservations.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </>
    )
}

export default TableauReservations;