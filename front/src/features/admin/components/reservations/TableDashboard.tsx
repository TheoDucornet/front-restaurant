import {Box, Grid, Typography} from "@mui/material";
import React, {useState} from "react";
import {AdminDico} from "../../dico/AdminDico";
import {TableDataType, TablesDataTypes} from "../../types/AdminTypes";
import TableButton from "./TableButton";
import ModalModifTable from "./ModalModifTable";

const TableDashboard = ({tables, deleteReserv, updateDispoTable}: TablesDataTypes) => {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [tableModalIndex, setTableModalIndex] = useState<number | undefined>(undefined);

    const changeTableModalIndex = (numTable: number) => {
        setTableModalIndex(tables.findIndex(table => table.numTable === numTable));
    }
    const getCurrentTableToDisplay = () => {
        if (tableModalIndex !== undefined)
            return tables[tableModalIndex];
        throw new Error("tableModalIndex is undefined, unexpected modal call");
    }

    return (
        <>
            <Typography
                variant="h4"
                component="h4"
                sx={{
                    fontFamily: 'monospace',
                    fontWeight: 700,
                    textDecoration: 'none',
                    marginTop: '20px',
                    textAlign: 'center',
                }}
            >
                {AdminDico.DASHBOARD_TITLE}
            </Typography>
            <Box sx={{marginTop: '30px', marginBottom: '10px'}}>
                <Grid container spacing={2}>
                    {tables.map(({numTable}, index) => (
                            <Grid item xs={3} key={index}>
                                <TableButton numTable={numTable} setIsModalOpen={setIsModalOpen}
                                             changeTableModalIndex={changeTableModalIndex}/>
                            </Grid>
                        )
                    )}
                </Grid>
            </Box>
            {(tableModalIndex !== undefined) && <ModalModifTable table={getCurrentTableToDisplay()} isModalOpen={isModalOpen}
                                                 setIsModalOpen={setIsModalOpen} updateDispoTable={updateDispoTable}
            deleteReserv={deleteReserv}/>}
        </>
    );
};

export default TableDashboard;
