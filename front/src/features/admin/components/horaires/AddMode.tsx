import {Box, Button, FormControl, Input, InputLabel, Typography} from "@mui/material";
import {AdminDico} from "../../dico/AdminDico";
import React from "react";
import dayjs from "dayjs";
import {LocalizationProvider, TimePicker} from "@mui/x-date-pickers";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";
import {NewHoraireModeDataTypes} from "../../types/AdminTypes";

type AddModeProps = {
    addHoraires: (newHoraire: NewHoraireModeDataTypes) => void;
}

const AddMode = ({addHoraires} : AddModeProps) => {

    const [newModeTitle, setNewModeTitle] = React.useState<string>();
    const [debutMidi, setDebutMidi] = React.useState<dayjs.Dayjs | null>();
    const [finMidi, setFinMidi] = React.useState<dayjs.Dayjs| null>();
    const [debutSoir, setDebutSoir] = React.useState<dayjs.Dayjs| null>();
    const [finSoir, setFinSoir] = React.useState<dayjs.Dayjs| null>();

    const checkMissingData = () => {
        return newModeTitle === undefined ||
            debutMidi === null || debutMidi === undefined ||
            finMidi === null || finMidi === undefined ||
            debutSoir === null || debutSoir === undefined ||
            finSoir === null || finSoir === undefined;
    }

    return (
        <>
            <Typography
                variant="h6"
                component="h6"
                sx={{
                    fontFamily: 'monospace',
                    fontWeight: 700,
                    textDecoration: 'none',
                    marginTop: '40px',
                    marginBottom: '5px',
                    textAlign: 'center',
                }}
            >
                {AdminDico.EDIT_HORAIRE}
            </Typography>
            <FormControl fullWidth>
                <InputLabel>Nouveau mode</InputLabel>
                <Input
                    value={newModeTitle}
                    placeholder="Nouveau mode"
                    onChange={(event) => {
                        setNewModeTitle(event.target.value)
                    }}
                />
                <Box sx={{marginTop: '15px', marginBottom: '20px'}}>
                    <Typography
                        variant="body1"
                        component="h6"
                        sx={{fontWeight: 700, marginBottom: '10px'}}
                    >
                        {AdminDico.EDIT_HORAIRE_MIDI}
                    </Typography>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <TimePicker
                            value={debutMidi}
                            onChange={(newValue) => {
                                setDebutMidi(newValue)
                            }}
                        />
                        <TimePicker
                            value={finMidi}
                            onChange={(newValue) => {
                                setFinMidi(newValue)
                            }}
                        />
                    </LocalizationProvider>
                </Box>
                <Box sx={{marginTop: '15px', marginBottom: '20px'}}>
                    <Typography
                        variant="body1"
                        component="h6"
                        sx={{fontWeight: 700, marginBottom: '10px'}}
                    >
                        {AdminDico.EDIT_HORAIRE_SOIR}
                    </Typography>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <TimePicker
                            value={debutSoir}
                            onChange={(newValue) => {
                                setDebutSoir(newValue)
                            }}
                        />
                        <TimePicker
                            value={finSoir}
                            onChange={(newValue) => {
                                setFinSoir(newValue)
                            }}
                        />
                    </LocalizationProvider>
                </Box>
                <Button
                    variant="contained"
                    color="primary"
                    disabled={checkMissingData()}
                    onClick={() => {
                        if (checkMissingData())
                            return;
                        const newModeData =
                            {
                                periode: newModeTitle || '',
                                debut_midi: debutMidi?.format('HH:mm:ss') || '',
                                fin_midi: finMidi?.format('HH:mm:ss') || '',
                                debut_soir: debutSoir?.format('HH:mm:ss') || '',
                                fin_soir: finSoir?.format('HH:mm:ss') || '',
                            }
                            addHoraires(newModeData);
                    }}
                >Valider</Button>
            </FormControl>
        </>
    )
}

export default AddMode;