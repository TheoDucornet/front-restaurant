import {Box, Button, FormControl, InputLabel, MenuItem, Select, Typography} from "@mui/material";
import {AdminDico} from "../../dico/AdminDico";
import React from "react";
import {HorairesTypes, NewHoraireServiceDataTypes} from "../../types/AdminTypes";
import {Dayjs} from "dayjs";
import {DatePicker, LocalizationProvider} from "@mui/x-date-pickers";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";

type SelectPeriodeProps = {
    horaires: HorairesTypes[],
    addHoraireService: (newHoraire: NewHoraireServiceDataTypes) => void,
}
const SelectPeriode = ({horaires, addHoraireService}: SelectPeriodeProps) => {

    const [currentHoraire, setCurrentHoraire] = React.useState<HorairesTypes>();
    const [debutPeriode, setDebutPeriode] = React.useState<Dayjs | null>();
    const [finPeriode, setFinPeriode] = React.useState<Dayjs | null>();

    function handleChange(e: { target: { value: any; }; }) {
        setCurrentHoraire(horaires.find(horaire => horaire.periode === e.target.value));
    }

    return (
        <>
            <Typography
                variant="h6"
                component="h6"
                sx={{
                    fontFamily: 'monospace',
                    fontWeight: 700,
                    textDecoration: 'none',
                    marginTop: '40px',
                    marginBottom: '5px',
                    textAlign: 'center',
                }}
            >
                {AdminDico.SELECT_HORAIRE}
            </Typography>
            <FormControl fullWidth>
                <InputLabel>Période</InputLabel>
                <Select
                    value={currentHoraire?.periode}
                    label="Période"
                    onChange={handleChange}
                >
                    {horaires.map((horaire) => (
                        <MenuItem value={horaire.periode}>{horaire.periode}</MenuItem>
                    ))
                    }
                </Select>
                {currentHoraire &&
                    <>
                        <Typography
                            variant="body1"
                            component="h6"
                            sx={{fontWeight: 700, marginBottom: '5px'}}
                        >
                            {'Heure du midi : ' + currentHoraire.debut_midi + ' - ' + currentHoraire.fin_midi}
                        </Typography>
                        <Typography
                            variant="body1"
                            component="h6"
                            sx={{fontWeight: 700, marginBottom: '10px'}}
                        >
                            {'Heure du soir : ' + currentHoraire.debut_soir + ' - ' + currentHoraire.fin_soir}
                        </Typography>
                        <Box sx={{marginTop: '15px', marginBottom: '20px'}}>
                            <Typography
                                variant="body1"
                                component="h6"
                                sx={{fontWeight: 700, marginBottom: '10px'}}
                            >
                                {AdminDico.SELECT_PERIODE}
                            </Typography>
                            <LocalizationProvider dateAdapter={AdapterDayjs}>
                                <DatePicker
                                    label={'Début'}
                                    value={debutPeriode}
                                    onChange={(newValue) => {
                                        setDebutPeriode(newValue)
                                    }}
                                />
                                <DatePicker
                                    label={'Fin'}
                                    value={finPeriode}
                                    onChange={(newValue) => {
                                        setFinPeriode(newValue)
                                    }}
                                />
                            </LocalizationProvider>
                        </Box>
                        <Button
                            variant="contained"
                            color="primary"
                            disabled={!debutPeriode || !finPeriode}
                            onClick={() => {
                                console.log(currentHoraire);
                                if (debutPeriode && finPeriode) {
                                    addHoraireService({
                                        "debut_midi": debutPeriode.format('YYYY-MM-DD') || '',
                                        "fin_midi": finPeriode.format('YYYY-MM-DD') || '',
                                        "mode": currentHoraire.periode
                                    });
                                }
                            }}
                        >Valider</Button>
                    </>
                }
            </FormControl>
        </>
    )
}

export default SelectPeriode;