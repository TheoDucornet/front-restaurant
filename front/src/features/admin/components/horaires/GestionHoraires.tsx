import {Typography} from "@mui/material";
import {AdminDico} from "../../dico/AdminDico";
import React from "react";
import {GestionHoraireTypes} from "../../types/AdminTypes";
import AddMode from "./AddMode";
import SelectPeriode from "./SelectPeriode";

const GestionHoraires = ({horaires, addHoraires, addHoraireService} : GestionHoraireTypes) => {

    return (
        <>
            <Typography
                variant="h4"
                component="h4"
                sx={{
                    fontFamily: 'monospace',
                    fontWeight: 700,
                    textDecoration: 'none',
                    marginTop: '40px',
                    marginBottom: '5px',
                    textAlign: 'center',
                }}
            >
                {AdminDico.GESTION_HORAIRES}
            </Typography>
            <SelectPeriode horaires={horaires} addHoraireService={addHoraireService}/>
            <AddMode addHoraires={addHoraires}/>
        </>
    )
}

export default GestionHoraires;