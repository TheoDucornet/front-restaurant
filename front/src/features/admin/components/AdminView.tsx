import {Box, Tab} from "@mui/material";
import React from "react";
import TableDashboard from "./reservations/TableDashboard";
import {AdminDataTypes} from "../types/AdminTypes";
import GestionReservation from "./reservations/GestionReservation";
import GestionHoraires from "./horaires/GestionHoraires";
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';

const AdminView = ({
                       tables,
                       deleteReserv,
                       updateDispoTable,
                       horaires,
                       addHoraires,
                       addHoraireService
                   }: AdminDataTypes) => {
    const [value, setValue] = React.useState('1');

    const handleChange = (event: React.SyntheticEvent, newValue: string) => {
        setValue(newValue);
    };
    return (
        <>
            <TabContext value={value}>
                <Box sx={{borderBottom: 1, borderColor: 'divider'}}>
                    <TabList onChange={handleChange}>
                        <Tab label="Gestion de réservations" value="1"/>
                        <Tab label="Gestion des horaires" value="2"/>
                    </TabList>
                </Box>
                <Box sx={{backgroundColor: '#f0f0f0', padding: '20px'}}>
                    <TabPanel value="1">
                        <TableDashboard tables={tables} updateDispoTable={updateDispoTable}
                                        deleteReserv={deleteReserv}/>
                        <GestionReservation tables={tables} deleteReserv={deleteReserv}/>
                    </TabPanel>
                    <TabPanel value="2">
                        <GestionHoraires horaires={horaires} addHoraires={addHoraires}
                                         addHoraireService={addHoraireService}/>
                    </TabPanel>
                </Box>
            </TabContext>


        </>
    );
};

export default AdminView;
