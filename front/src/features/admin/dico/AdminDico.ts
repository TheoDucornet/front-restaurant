export enum AdminDico {
    DASHBOARD_TITLE = "Dashboard",
    POSSIBILITE_RESERVATION = "Possibilité de réserver :",
    DETAIL_TABLE = "Détails de la table ",
    RESERVATIONS = "Réservations",
    RESERVATIONS_EN_COURS = "Réservations en cours",
    GESTION_HORAIRES = "Gestion des horaires",
    EDIT_HORAIRE = "Ajouter un mode horaire",
    EDIT_HORAIRE_MIDI = "Sélectionner l'horaire du midi",
    EDIT_HORAIRE_SOIR = "Sélectionner l'horaire du soir",
    SELECT_HORAIRE = "Sélectionner une période avec un horaire particulier",
    SELECT_PERIODE = "Choisissez la période où appliquer ce mode",
}