import AdminView from "../components/AdminView";
import HttpService from "../../../http/HttpService";
import {useEffect, useState} from "react";
import {HorairesTypes, NewHoraireModeDataTypes, NewHoraireServiceDataTypes, TableDataType} from "../types/AdminTypes";

const AdminPage = () => {

    const apiService = new HttpService();

    const [tables, setTables] = useState<TableDataType[]>([]);

    const [horaires, setHoraires] = useState<HorairesTypes[]>([]);

    const getReservs = (): void => {
        apiService.get("/admin/salle").then(({request}) => {
            request
                .then((response) => {
                    setTables(response.data.sort((a: { numTable: number; }, b: {
                        numTable: number;
                    }) => a.numTable - b.numTable));
                })
                .catch((error) => {
                    console.error(error)
                    alert('Accès au backend échoué');
                });
        });
    }

    const deleteReserv = (id: number): void => {
        apiService.delete("/reserv/" + id).then(({request}) => {
            request.then((response) => {
                getReservs();
                alert("Réservation supprimée avec succès")
            })
                .catch((error) => {
                    alert(error);
                    console.error(error);
                });
        });
    };

    const updateDispoTable = (id: number): void => {
        apiService.put("/admin/table/dispo/" + id, {}).then(({request}) => {
            request.then((response) => {
                getReservs();
                alert("Disponibilité de la table modifiée avec succès")
            })
                .catch((error) => {
                    alert(error);
                    console.error(error);
                });
        });
    };

    const getHoraires = (): void => {
        apiService.get("/admin/horaires").then(({request}) => {
            request.then((response) => {
                setHoraires(response.data);
            })
                .catch((error) => {
                    alert(error);
                    console.error(error);
                });
        });
    };

    const addHoraires = (newHoraire : NewHoraireModeDataTypes): void => {
        console.log(newHoraire);
        apiService.post("/admin/horaires", newHoraire).then(({request}) => {
            request.then((response) => {
                getHoraires();
                alert("Mode horaire ajoutés avec succès")
            })
                .catch((error) => {
                    alert(error);
                    console.error(error);
                });
        });
    };

    const addHoraireService = (selectedPeriode : NewHoraireServiceDataTypes): void => {
        console.log(selectedPeriode);
        apiService.post("/admin/horaires/service", selectedPeriode).then(({request}) => {
            request.then((response) => {
                getHoraires();
                alert("Période horaire ajoutée avec succès");
            })
                .catch((error) => {
                    alert(error);
                    console.error(error);
                });
        });
    };

    useEffect(() => {
        getReservs();
        getHoraires();
    }, []);

    return (
        <AdminView tables={tables} horaires={horaires}
                   deleteReserv={deleteReserv} updateDispoTable={updateDispoTable}
                   addHoraires={addHoraires} addHoraireService={addHoraireService}
        />
    );
}

export default AdminPage;