import axios from 'axios';
import {
	createContext,
	useContext,
	useEffect,
	useMemo,
	useReducer,
} from 'react';
import Cookies from "universal-cookie";

interface TokenContextType {
	state: TokenState;
	setToken: (newToken: string) => void;
	clearToken: () => void;
}

const AuthContext = createContext<TokenContextType | undefined>(undefined);

interface ProviderProps {
	children: React.ReactNode;
}

interface TokenState {
	token: string | null;
}

type TokenAction =
	| { type: 'SET_TOKEN'; payload: string }
	| { type: 'CLEAR_TOKEN' };

const authReducer = (state: TokenState, action: TokenAction): TokenState => {
	const cookies = new Cookies();
	switch (action.type) {
		case 'SET_TOKEN':
			axios.defaults.headers.common['Authorization'] =
				'Bearer ' + action.payload;
			cookies.set('token', action.payload);

			return { ...state, token: action.payload };
		case 'CLEAR_TOKEN':
			delete axios.defaults.headers.common['Authorization'];
			cookies.remove('token');

			return { ...state, token: null };
		default:
			return { ...state };
	}
};

const AuthProvider = ({ children }: ProviderProps) => {
	const cookies = new Cookies();
	const [state, dispatch] = useReducer(authReducer, { token: cookies.get('token') });

	const setToken = (newToken: string) => {
		dispatch({ type: 'SET_TOKEN', payload: newToken });
	};

	const clearToken = () => {
		dispatch({ type: 'CLEAR_TOKEN' });
	};

	useEffect(() => {
		const storedToken = cookies.get('token');
		if (storedToken) dispatch({ type: 'SET_TOKEN', payload: storedToken });
	}, []);

	useEffect(() => {
		if (state.token) {
			axios.defaults.headers.common['Authorization'] =
				'Bearer ' + state.token;
			cookies.set('token', state.token);
		} else {
			delete axios.defaults.headers.common['Authorization'];
			cookies.remove('token');
		}
	}, [state.token]);

	const contextValue = useMemo(
		() => ({
			state,
			setToken,
			clearToken,
		}),
		[state]
	);

	return (
		<AuthContext.Provider value={contextValue}>
			{children}
		</AuthContext.Provider>
	);
};

export const useAuthContext = (): TokenContextType => {
	const context = useContext(AuthContext);

	if (!context) {
		throw new Error('useTokenContext must be used within a TaskProvider');
	}

	return context;
};

export default AuthProvider;
