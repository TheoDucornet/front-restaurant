import { Box, Input, Typography } from '@mui/material';
import React from 'react';
import { LoginDico } from '../dico/LoginDico';
import { CredsInputPropsType } from '../types/AuthentTypes';

const CredsInput = ({ handleChange }: CredsInputPropsType) => {
	return (
		<>
			<Box sx={{ marginTop: '30px', marginBottom: '10px' }}>
				<Typography
					variant="body1"
					component="h5"
					sx={{ fontWeight: 700 }}
				>
					{LoginDico.LOGIN}
				</Typography>
				<Input
					placeholder={LoginDico.LOGIN}
					fullWidth
					onChange={(e) => handleChange('login', e.target.value)}
				/>
			</Box>
			<Box sx={{ marginTop: '30px', marginBottom: '10px' }}>
				<Typography
					variant="body1"
					component="h5"
					sx={{ fontWeight: 700 }}
				>
					{LoginDico.PWD}
				</Typography>
				<Input
					placeholder={LoginDico.PWD}
					fullWidth
					onChange={(e) => handleChange('pwd', e.target.value)}
				/>
			</Box>
		</>
	);
};

export default CredsInput;
