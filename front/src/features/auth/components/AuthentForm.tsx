import { Button } from '@mui/material';
import React, { useState } from 'react';
import { LoginDico } from '../dico/LoginDico';
import CredsInput from './CredsInput';
import { AuthentFormPropsType, Creds } from '../types/AuthentTypes';

const AuthentForm = ({ handleLogin }: AuthentFormPropsType) => {
	const [creds, setCreds] = useState<Creds>({ login: '', pwd: '' });

	const handleChange = (key: string, value: string) => {
		setCreds({ ...creds, [key]: value });
	};

	const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		handleLogin(creds);
	};

	return (
		<form onSubmit={handleSubmit}>
			<CredsInput handleChange={handleChange} />
			<Button type="submit" variant="contained" color="primary">
				{LoginDico.SUBMIT}
			</Button>
		</form>
	);
};

export default AuthentForm;
