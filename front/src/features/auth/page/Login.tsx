import {useNavigate} from 'react-router-dom';
import AuthentForm from '../components/AuthentForm';
import {useAuthContext} from '../context/AuthProvider';
import {Creds} from '../types/AuthentTypes';
import HttpService from "../../../http/HttpService";

const Login = () => {
    const apiService = new HttpService();
    const {setToken} = useAuthContext();
    const navigate = useNavigate();

    const login = async ({login, pwd}: Creds) => {
        apiService.login(login, pwd).then(({request}) => {
        	request.then((response) => {
        		setToken(response.data);
                navigate("/admin");
        	})
        		.catch((error) => {
        			console.error(error);
        		});
        });
    };

    return <AuthentForm handleLogin={login}/>;
};

export default Login;
