import { useNavigate } from 'react-router-dom';
import { useAuthContext } from '../context/AuthProvider';

const Logout = () => {
	const { clearToken } = useAuthContext();
	const navigate = useNavigate();

	const handleLogout = () => {
		clearToken();
		navigate('/', { replace: true });
	};

	setTimeout(() => {
		handleLogout();
	}, 3 * 1000);

	return <>Logging out...</>;
};

export default Logout;
