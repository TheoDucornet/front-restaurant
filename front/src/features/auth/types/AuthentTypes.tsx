export interface Creds {
	login: string;
	pwd: string;
}

export interface CredsInputPropsType {
	handleChange: (key: string, value: string) => void;
}

export interface AuthentFormPropsType {
	handleLogin: ({ login, pwd }: Creds) => void;
}
