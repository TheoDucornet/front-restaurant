package com.example.backend.exceptions;

public class ParamHoraireNulException extends Exception {

    public ParamHoraireNulException() {
        super("Les paramètres horaires ne peuvent pas être nuls.");
    }
}
