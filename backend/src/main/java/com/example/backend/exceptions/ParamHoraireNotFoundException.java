package com.example.backend.exceptions;

public class ParamHoraireNotFoundException extends Exception {
    public ParamHoraireNotFoundException() {
        super("Pas d'horaire trouvée pour cette période.");
    }
}
