package com.example.backend.exceptions;

public class ReservNotFoundException extends Exception{

    public ReservNotFoundException(){
        super("La réservation n'existe pas");
    }
}
