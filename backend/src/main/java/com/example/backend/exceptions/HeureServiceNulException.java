package com.example.backend.exceptions;

public class HeureServiceNulException extends Exception {

    public HeureServiceNulException() {
        super("Les paramètres du nouveau service ne peuvent pas être nuls.");
    }
}
