package com.example.backend.exceptions;

public class ReservNullException extends Exception{

    public ReservNullException() {
        super("Une ou plusieurs informations sont manquantes pour la réservation.");
    }
}
