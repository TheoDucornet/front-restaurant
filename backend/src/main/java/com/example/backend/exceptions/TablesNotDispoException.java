package com.example.backend.exceptions;

public class TablesNotDispoException extends Exception{

    public TablesNotDispoException() {
        super("Aucune table n'est pas disponible");

    }
}
