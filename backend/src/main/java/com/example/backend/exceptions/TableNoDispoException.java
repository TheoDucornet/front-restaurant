package com.example.backend.exceptions;

public class TableNoDispoException extends Exception {

    public TableNoDispoException(String message) {
        super("La table " + message + " n'est pas disponible");

    }
}
