package com.example.backend.exceptions;

public class TableMaxPersonException extends Exception {

    public TableMaxPersonException() {
        super("Pas de table disponible pour le nombre de personnes demandé");

    }
}
