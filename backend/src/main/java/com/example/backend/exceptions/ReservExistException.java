package com.example.backend.exceptions;

public class ReservExistException extends Exception{

        public ReservExistException(String message) {
            super("Une réservation existe déjà pour la table "+ message+ " à cette date et heure.");

        }
}
