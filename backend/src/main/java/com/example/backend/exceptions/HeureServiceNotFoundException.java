package com.example.backend.exceptions;

public class HeureServiceNotFoundException extends Exception {
    public HeureServiceNotFoundException() {
        super("Pas service trouvé.");
    }
}
