package com.example.backend.model.dto.out;

import java.time.LocalTime;
import java.util.List;

public record HoraireDTO (String periode, LocalTime debut_midi, LocalTime fin_midi, LocalTime debut_soir, LocalTime fin_soir  ){
}
