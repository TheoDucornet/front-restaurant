package com.example.backend.model.dao;

import com.example.backend.model.entities.ParamHeureServiceEntity;
import com.example.backend.model.entities.TablesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ParamHeureServiceRepository extends JpaRepository<ParamHeureServiceEntity, Long> {

    Optional<ParamHeureServiceEntity> findByPeriode(String periode);
}
