package com.example.backend.model.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "reserv_table")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(ReservTableId.class)
public class ReservTableEntity {

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="num_table", referencedColumnName="num_table")
    private TablesEntity table;

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="id_reserv", referencedColumnName="id")
    private ReservEntity reserv;

}

