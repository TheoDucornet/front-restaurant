package com.example.backend.model.mapper;

import com.example.backend.model.dto.out.HoraireDTO;
import com.example.backend.model.entities.ParamHeureServiceEntity;

public class HoraireMapper {

    public static HoraireDTO horaireServiceToDTO (ParamHeureServiceEntity entity){
        return new HoraireDTO(entity.getPeriode(), entity.getDebut_midi(), entity.getFin_midi(), entity.getDebut_soir(), entity.getFin_soir());
    }

}
