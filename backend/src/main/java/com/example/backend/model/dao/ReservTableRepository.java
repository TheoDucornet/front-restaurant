package com.example.backend.model.dao;

import com.example.backend.model.entities.ReservEntity;
import com.example.backend.model.entities.ReservTableEntity;
import com.example.backend.model.entities.ReservTableId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservTableRepository extends JpaRepository<ReservTableEntity, ReservTableId> {

    List<ReservTableEntity> findByReserv(ReservEntity reservEntity);
}
