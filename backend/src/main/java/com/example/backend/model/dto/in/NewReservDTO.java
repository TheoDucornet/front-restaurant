package com.example.backend.model.dto.in;

import java.time.LocalDateTime;

public record NewReservDTO (LocalDateTime date, String email, String tel, String prenom, String nom, int nb_personnes) {

}
