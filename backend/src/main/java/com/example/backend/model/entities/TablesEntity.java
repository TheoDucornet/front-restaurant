package com.example.backend.model.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;

@Table(name = "tables")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TablesEntity {

    @Id
    @Column(name = "num_table", nullable = false)
    private Integer numTable;

    @Column(name = "dispo", nullable = false)
    private boolean dispo = true;

    @Column(name = "nb_places", nullable = false)
    private Integer nbPlaces;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "table")
    private Collection<ReservTableEntity> reservs;


}
