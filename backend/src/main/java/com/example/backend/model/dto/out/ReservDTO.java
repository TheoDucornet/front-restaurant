package com.example.backend.model.dto.out;

import java.time.LocalDateTime;

public record ReservDTO (Integer id, LocalDateTime date, String email, String tel, String prenom, String nom, Integer nb_personnes) {
}
