package com.example.backend.model.dao;

import com.example.backend.model.entities.ParamReservEntity;
import com.example.backend.model.entities.TablesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParamReservRepository extends JpaRepository<ParamReservEntity, Long> {

    ParamReservEntity findFirstByOrderByIdAsc();
}
