package com.example.backend.model.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalTime;
import java.util.Collection;

@Table(name = "param_heure_service")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class ParamHeureServiceEntity {

    @Id
    @Column(name = "periode", nullable = false, length = 50)
    private String periode ;

    @Column(name = "debut_midi", nullable = false)
    private LocalTime debut_midi;

    @Column(name = "fin_midi", nullable = false)
    private LocalTime fin_midi;

    @Column(name = "debut_soir", nullable = false)
    private LocalTime debut_soir;

    @Column(name = "fin_soir", nullable = false)
    private LocalTime fin_soir;

    @OneToMany(mappedBy = "mode")
    private Collection<HeureServiceEntity> heure_services;
}
