package com.example.backend.model.mapper;

import com.example.backend.model.dto.out.ReservDTO;
import com.example.backend.model.entities.ReservEntity;
import com.example.backend.model.entities.ReservTableEntity;

public class ReservMapper {

    public static ReservDTO reservtoDTO(ReservTableEntity entity) {
        ReservEntity reserv= entity.getReserv();

        return new ReservDTO(reserv.getId(), reserv.getDate(), reserv.getEmail(), reserv.getTel(), reserv.getPrenom(), reserv.getNom(), reserv.getNb_personnes());
    }
}
