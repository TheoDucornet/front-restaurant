package com.example.backend.model.mapper;

import com.example.backend.model.dto.out.HeureServiceDTO;
import com.example.backend.model.entities.HeureServiceEntity;

public class HeureServiceMapper {

    public static HeureServiceDTO horaireServiceToDTO(HeureServiceEntity entity) {
        return new HeureServiceDTO(entity.getId(), entity.getDateDebut(), entity.getDateFin(), HoraireMapper.horaireServiceToDTO(entity.getMode()));
    }
}
