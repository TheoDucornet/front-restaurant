package com.example.backend.model.dto.in;

import java.time.LocalTime;

public record NewHoraireDTO(String periode, LocalTime debut_midi, LocalTime fin_midi, LocalTime debut_soir,
                            LocalTime fin_soir) {
}
