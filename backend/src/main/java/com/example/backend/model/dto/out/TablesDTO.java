package com.example.backend.model.dto.out;


import java.util.List;

public record TablesDTO (Integer numTable, Boolean dispo, Integer nb_places, List<ReservDTO> reservs) {
}
