package com.example.backend.model.dto.in;

import java.time.LocalDate;

public record NewHoraireServiceDTO(LocalDate debut_midi, LocalDate fin_midi, String mode) {
}
