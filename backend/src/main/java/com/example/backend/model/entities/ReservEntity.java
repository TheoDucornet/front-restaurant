package com.example.backend.model.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Collection;


@Table(name = "reserv")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class ReservEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "date", nullable = false)
    private LocalDateTime date;

    @Column(name = "email", nullable = false, length = 100)
    private String email;

    @Column(name = "tel", nullable = false, length = 50)
    private String tel;

    @Column(name = "prenom", nullable = false, length = 50)
    private String prenom;

    @Column(name = "nom", nullable = false, length = 50)
    private String nom;

    @Column(name = "nb_personnes", nullable = false)
    private Integer nb_personnes;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, orphanRemoval = true, mappedBy = "reserv")
    private Collection<ReservTableEntity> tables;
}
