package com.example.backend.model.dao;

import com.example.backend.model.entities.HeureServiceEntity;
import com.example.backend.model.entities.TablesEntity;
import jakarta.persistence.criteria.CriteriaBuilder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface HeureServiceRepository extends JpaRepository<HeureServiceEntity, Long> {

    Optional<HeureServiceEntity> findByDateDebutIsBeforeAndDateFinIsAfter(LocalDate dateDebut, LocalDate dateFin);

    Optional<HeureServiceEntity> findById(Integer id);
}
