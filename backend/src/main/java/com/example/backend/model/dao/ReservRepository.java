package com.example.backend.model.dao;

import com.example.backend.model.entities.ReservEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ReservRepository extends JpaRepository<ReservEntity, Long> {
    Optional<ReservEntity> findById(Integer id);

    void delete(ReservEntity reservEntity);
}
