package com.example.backend.model.mapper;

import com.example.backend.model.dto.out.TablesDTO;
import com.example.backend.model.entities.TablesEntity;


public class TablesMapper {

    public static TablesDTO tablestoDTO(TablesEntity entity) {
        return new TablesDTO(entity.getNumTable(),
                entity.isDispo(),
                entity.getNbPlaces(),
                entity.getReservs()
                        .stream()
                        .map(ReservMapper::reservtoDTO)
                        .toList()
        );
    }
}
