package com.example.backend.model.dto.out;

import java.time.LocalDate;

public record HeureServiceDTO(Integer id, LocalDate debut_midi, LocalDate fin_midi, HoraireDTO mode) {

}
