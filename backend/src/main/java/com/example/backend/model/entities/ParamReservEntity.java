package com.example.backend.model.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalTime;

@Table(name = "param_reserv")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class ParamReservEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "duree_reservation", nullable = false)
    private LocalTime duree_reservation;
}
