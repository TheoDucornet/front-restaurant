package com.example.backend.model.dao;

import com.example.backend.model.entities.TablesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface TablesRepository extends JpaRepository<TablesEntity, Long> {


    Optional<TablesEntity> findByNumTable(Integer s);

    Optional<Collection<TablesEntity>> findByDispoIsTrue();
}
