package com.example.backend.services.heureService;

import com.example.backend.exceptions.HeureServiceNulException;
import com.example.backend.exceptions.HeureServiceNotFoundException;
import com.example.backend.exceptions.ParamHoraireNotFoundException;
import com.example.backend.model.dao.HeureServiceRepository;
import com.example.backend.model.dao.ParamHeureServiceRepository;
import com.example.backend.model.dto.in.NewHoraireServiceDTO;
import com.example.backend.model.dto.out.HeureServiceDTO;
import com.example.backend.model.entities.HeureServiceEntity;
import com.example.backend.model.entities.ParamHeureServiceEntity;
import com.example.backend.model.mapper.HeureServiceMapper;
import com.example.backend.services.paramHeureService.ParamHeureService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SQLHeureService implements HeureService {
    private final HeureServiceRepository heureServiceRepository;
    private final ParamHeureServiceRepository paramHeureServiceRepository;

    @Override
    public Optional<HeureServiceEntity> addhoraireServie(NewHoraireServiceDTO newHeureServiceDTO) throws HeureServiceNulException, ParamHoraireNotFoundException {
        if (newHeureServiceDTO.debut_midi() == null
                || newHeureServiceDTO.fin_midi() == null
                || newHeureServiceDTO.mode().isEmpty()) {
            throw new HeureServiceNulException();
        }
        Optional<ParamHeureServiceEntity> paramHeureService = paramHeureServiceRepository.findByPeriode(newHeureServiceDTO.mode());

        if (paramHeureService.isEmpty()) {
            throw new ParamHoraireNotFoundException();
        }

        HeureServiceEntity heureServiceEntity = new HeureServiceEntity();

        heureServiceEntity.setDateDebut(newHeureServiceDTO.debut_midi());
        heureServiceEntity.setDateFin(newHeureServiceDTO.fin_midi());
        heureServiceEntity.setMode(paramHeureService.get());

        heureServiceRepository.save(heureServiceEntity);

        return Optional.of(heureServiceEntity);

    }

    @Override
    public List<HeureServiceDTO> findAll() {

        return heureServiceRepository.findAll().stream().map(HeureServiceMapper::horaireServiceToDTO).toList();
    }

    @Override
    public void deleteHoraireService(Integer id) throws HeureServiceNotFoundException {
        HeureServiceEntity heureServiceEntity = heureServiceRepository.findById(id)
                .orElseThrow(HeureServiceNotFoundException::new);
        heureServiceRepository.delete(heureServiceEntity);
    }

    @Override
    public HeureServiceDTO updateHoraireService(Integer id, NewHoraireServiceDTO newHoraireServiceDTO) throws HeureServiceNotFoundException, ParamHoraireNotFoundException {
        HeureServiceEntity heureServiceEntity = heureServiceRepository.findById(id)
                .orElseThrow(HeureServiceNotFoundException::new);

        ParamHeureServiceEntity paramHeureServiceEntity = paramHeureServiceRepository.findByPeriode(newHoraireServiceDTO.mode())
                .orElseThrow(ParamHoraireNotFoundException::new);

        heureServiceEntity.setDateDebut(newHoraireServiceDTO.debut_midi());
        heureServiceEntity.setDateFin(newHoraireServiceDTO.fin_midi());
        heureServiceEntity.setMode(paramHeureServiceEntity);

        heureServiceRepository.save(heureServiceEntity);

        return HeureServiceMapper.horaireServiceToDTO(heureServiceEntity);
    }
}
