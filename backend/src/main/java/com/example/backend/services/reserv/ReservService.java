package com.example.backend.services.reserv;

import com.example.backend.exceptions.*;
import com.example.backend.model.dto.in.NewReservDTO;
import com.example.backend.model.dto.out.HoraireDTO;
import com.example.backend.model.dto.out.ReservDTO;
import com.example.backend.model.entities.ReservEntity;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collection;
import java.util.Optional;

public interface ReservService {
    public Optional<ReservEntity> addReserv(NewReservDTO newReservDTO) throws ReservNullException, TableNotFoundExcepetion, ReservExistException, TableNoDispoException, TableMaxPersonException, TablesNotDispoException;

    public Optional<ReservEntity> findById(Integer id);

    public HoraireDTO getHoraire(LocalDate date) throws ParamHoraireNotFoundException;

    public void deleterReserv(Integer id) throws ReservNotFoundException;

    ReservEntity updateReserv(Integer id, NewReservDTO newReserv) throws ReservNotFoundException, ReservNullException, TablesNotDispoException, TableMaxPersonException;
}
