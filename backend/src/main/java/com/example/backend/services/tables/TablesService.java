package com.example.backend.services.tables;

import com.example.backend.exceptions.TableNotFoundExcepetion;
import com.example.backend.model.dto.out.TablesDTO;
import com.example.backend.model.entities.TablesEntity;

import java.util.List;

public interface TablesService {
    public List<TablesDTO> findAll();
    public TablesDTO updateTableDispo(Integer numTable) throws TableNotFoundExcepetion;
}
