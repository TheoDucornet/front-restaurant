package com.example.backend.services.reserv;

import com.example.backend.exceptions.*;
import com.example.backend.model.dao.*;
import com.example.backend.model.dto.in.NewReservDTO;
import com.example.backend.model.dto.out.HoraireDTO;
import com.example.backend.model.dto.out.ReservDTO;
import com.example.backend.model.entities.*;
import com.example.backend.model.mapper.HoraireMapper;
import com.example.backend.model.mapper.ReservMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.swing.text.html.parser.Entity;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SQLReservService implements ReservService {

    private final TablesRepository tablesRepository;
    private final ReservRepository reservRepository;
    private final ParamReservRepository paramReservRepository;
    private final HeureServiceRepository heureServiceRepository;
    private final ParamHeureServiceRepository paramHeureServiceRepository;
    private final ReservTableRepository reservTableRepository;

    @Override
    public Optional<ReservEntity> addReserv(NewReservDTO newReservDTO) throws ReservNullException, TableNotFoundExcepetion, ReservExistException, TableNoDispoException, TableMaxPersonException, TablesNotDispoException {

        // verifiction des champs obligatoires
        if (newReservDTO.date() == null ||
                newReservDTO.email() == null || newReservDTO.email().isEmpty() ||
                newReservDTO.tel() == null || newReservDTO.tel().isEmpty() ||
                newReservDTO.prenom() == null || newReservDTO.prenom().isEmpty() ||
                newReservDTO.nom() == null || newReservDTO.nom().isEmpty() ||
                newReservDTO.nb_personnes() <= 0) {
            throw new ReservNullException();
        }

        // Récupération de la duré d'une réservation
        ParamReservEntity param = paramReservRepository.findFirstByOrderByIdAsc();
        LocalTime time = param.getDuree_reservation();
        LocalDateTime dateFin = newReservDTO.date().plusHours(time.getHour()).plusMinutes(time.getMinute());
        LocalDateTime dateDebut = newReservDTO.date().minusHours(time.getHour()).minusMinutes(time.getMinute());

        // Récupération des tables disponibles pour la reservation
        Optional<Collection<TablesEntity>> tablesEntityList = tablesRepository.findByDispoIsTrue();

        if (tablesEntityList.isEmpty()) {
            throw new TablesNotDispoException();
        }

        Collection<TablesEntity> tablesDispoList = tablesEntityList.get().stream()
                .filter(table ->
                        (table.getReservs().stream()
                                .filter(tableReserv ->
                                        tableReserv.getReserv().getDate().isAfter(dateDebut) && tableReserv.getReserv().getDate().isBefore(dateFin)
                                )
                                .toList()
                        ).isEmpty()

                )
                .toList();
        if (tablesDispoList.isEmpty()) {
            throw new TablesNotDispoException();
        }
        Integer maxPlaces = tablesDispoList.stream()
                .map(TablesEntity::getNbPlaces)
                .reduce(0, Integer::sum);

        if (maxPlaces < newReservDTO.nb_personnes()) {
            throw new TableMaxPersonException();
        }

        // Enregistrement de la reservation
        ReservEntity reserv = new ReservEntity();
        reserv.setDate(newReservDTO.date());
        reserv.setEmail(newReservDTO.email());
        reserv.setTel(newReservDTO.tel());
        reserv.setPrenom(newReservDTO.prenom());
        reserv.setNom(newReservDTO.nom());
        reserv.setNb_personnes(newReservDTO.nb_personnes());
        reservRepository.save(reserv);

        // Enregistrement des tables réservées
        Integer NbPersonne = newReservDTO.nb_personnes();
        Integer i = 0;
        while (NbPersonne > 0) {
            TablesEntity table = tablesDispoList.stream().skip(i).findFirst().get();
            ReservTableEntity reservTable = new ReservTableEntity();
            reservTable.setReserv(reserv);
            reservTable.setTable(table);
            reservTableRepository.save(reservTable);
            NbPersonne -= table.getNbPlaces();
            i++;
        }
        ;
        return Optional.of(reserv);
    }

    @Override
    public Optional<ReservEntity> findById(Integer id) {
        return reservRepository.findById(id);
    }

    @Override
    public HoraireDTO getHoraire(LocalDate date) throws ParamHoraireNotFoundException {

        Optional<HeureServiceEntity> heureService = heureServiceRepository.findByDateDebutIsBeforeAndDateFinIsAfter(date.plusDays(1), date.minusDays(1));

        if (heureService.isPresent()) {
            return HoraireMapper.horaireServiceToDTO(heureService.get().getMode());
        } else {
            Optional<ParamHeureServiceEntity> paramHeureService = paramHeureServiceRepository.findByPeriode(date.format(DateTimeFormatter.ofPattern("EEEE")));

            if (paramHeureService.isEmpty()) {
                throw new ParamHoraireNotFoundException();
            }
            return HoraireMapper.horaireServiceToDTO(paramHeureService.get());

        }

    }

    @Override
    public void deleterReserv(Integer id) throws ReservNotFoundException {
        Optional<ReservEntity> reservEntity = reservRepository.findById(id);

        if (reservEntity.isEmpty()) {
            throw new ReservNotFoundException();
        }
        reservRepository.delete(reservEntity.get());
    }

    @Override
    public ReservEntity updateReserv(Integer id, NewReservDTO newReservDTO) throws ReservNotFoundException, ReservNullException, TablesNotDispoException, TableMaxPersonException {

        Optional<ReservEntity> reservEntity = reservRepository.findById(id);
        if (reservEntity.isEmpty()) {
            throw new ReservNotFoundException();
        }

        // verifiction des champs obligatoires
        if (newReservDTO.date() == null ||
                newReservDTO.email() == null || newReservDTO.email().isEmpty() ||
                newReservDTO.tel() == null || newReservDTO.tel().isEmpty() ||
                newReservDTO.prenom() == null || newReservDTO.prenom().isEmpty() ||
                newReservDTO.nom() == null || newReservDTO.nom().isEmpty() ||
                newReservDTO.nb_personnes() <= 0) {
            throw new ReservNullException();
        }

        List<ReservTableEntity> reservTable = reservTableRepository.findByReserv(reservEntity.get());


        Integer maxPlaces = reservTable.stream()
                .map(reservTableEntity -> reservTableEntity.getTable().getNbPlaces())
                .reduce(0, Integer::sum);

        // Update de la reservation
        ReservEntity reserv = reservEntity.get();
        reserv.setDate(newReservDTO.date());
        reserv.setEmail(newReservDTO.email());
        reserv.setTel(newReservDTO.tel());
        reserv.setPrenom(newReservDTO.prenom());
        reserv.setNom(newReservDTO.nom());
        reserv.setNb_personnes(newReservDTO.nb_personnes());

        if (maxPlaces < newReservDTO.nb_personnes()) {

            // Récupération de la duré d'une réservation
            ParamReservEntity param = paramReservRepository.findFirstByOrderByIdAsc();
            LocalTime time = param.getDuree_reservation();
            LocalDateTime dateFin = newReservDTO.date().plusHours(time.getHour()).plusMinutes(time.getMinute());
            LocalDateTime dateDebut = newReservDTO.date().minusHours(time.getHour()).minusMinutes(time.getMinute());

            // Récupération des tables disponibles pour la reservation
            Optional<Collection<TablesEntity>> tablesEntityList = tablesRepository.findByDispoIsTrue();

            if (tablesEntityList.isEmpty()) {
                throw new TablesNotDispoException();
            }

            Collection<TablesEntity> tablesDispoList = tablesEntityList.get().stream()
                    .filter(table ->
                            (table.getReservs().stream()
                                    .filter(tableReserv ->
                                            tableReserv.getReserv().getDate().isAfter(dateDebut) && tableReserv.getReserv().getDate().isBefore(dateFin)
                                    )
                                    .toList()
                            ).isEmpty()

                    )
                    .toList();
            if (tablesDispoList.isEmpty()) {
                throw new TablesNotDispoException();
            }
            Integer maxPlacesTablesDispo = tablesDispoList.stream()
                    .map(TablesEntity::getNbPlaces)
                    .reduce(0, Integer::sum);

            if ((maxPlacesTablesDispo + maxPlaces) < newReservDTO.nb_personnes()) {
                throw new TableMaxPersonException();
            }


            // Enregistrement des tables réservées
            Integer NbPersonne = newReservDTO.nb_personnes() - maxPlaces;
            Integer i = 0;
            while (NbPersonne > 0) {
                TablesEntity table = tablesDispoList.stream().skip(i).findFirst().get();
                ReservTableEntity newreservTable = new ReservTableEntity();
                newreservTable.setReserv(reserv);
                newreservTable.setTable(table);
                reservTableRepository.save(newreservTable);
                NbPersonne -= table.getNbPlaces();
                i++;
            }
            ;


        }

        reservRepository.save(reserv);

        return reserv;

    }
}

