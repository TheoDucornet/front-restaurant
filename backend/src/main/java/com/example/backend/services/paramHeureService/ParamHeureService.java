package com.example.backend.services.paramHeureService;

import com.example.backend.exceptions.ParamHoraireNotFoundException;
import com.example.backend.exceptions.ParamHoraireNulException;
import com.example.backend.model.dto.in.NewHoraireDTO;
import com.example.backend.model.dto.out.HoraireDTO;
import com.example.backend.model.entities.ParamHeureServiceEntity;

import java.util.List;
import java.util.Optional;

public interface ParamHeureService {
    List<HoraireDTO> findAll();

    Optional<ParamHeureServiceEntity> addHoraire(NewHoraireDTO newHoraireDTO) throws ParamHoraireNulException;

    HoraireDTO findHoraire(String periode) throws ParamHoraireNotFoundException;

}
