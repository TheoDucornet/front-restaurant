package com.example.backend.services.tables;

import com.example.backend.exceptions.TableNotFoundExcepetion;
import com.example.backend.model.dao.TablesRepository;
import com.example.backend.model.dto.out.TablesDTO;
import com.example.backend.model.entities.TablesEntity;
import com.example.backend.model.mapper.TablesMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SQLTablesServies implements TablesService{
    private final TablesRepository tablesRepository;
    @Override
    public List<TablesDTO> findAll() {
        return tablesRepository.findAll().stream().map(TablesMapper::tablestoDTO).toList();
    }

    @Override
    public TablesDTO updateTableDispo(Integer numTable) throws TableNotFoundExcepetion {
        Optional<TablesEntity> table = tablesRepository.findByNumTable(numTable);
        if (table.isEmpty()){
            throw new TableNotFoundExcepetion(numTable.toString());
        }
        else{
            TablesEntity tableEntity = table.get();
            tableEntity.setDispo(!tableEntity.isDispo());
            tablesRepository.save(tableEntity);
            return TablesMapper.tablestoDTO(tableEntity);
        }

    }
}
