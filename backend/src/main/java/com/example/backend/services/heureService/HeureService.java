package com.example.backend.services.heureService;

import com.example.backend.exceptions.HeureServiceNotFoundException;
import com.example.backend.exceptions.HeureServiceNulException;
import com.example.backend.exceptions.ParamHoraireNotFoundException;
import com.example.backend.model.dto.in.NewHoraireServiceDTO;
import com.example.backend.model.dto.out.HeureServiceDTO;
import com.example.backend.model.entities.HeureServiceEntity;

import java.util.List;
import java.util.Optional;

public interface HeureService {

    Optional<HeureServiceEntity> addhoraireServie(NewHoraireServiceDTO newHeureServiceDTO) throws HeureServiceNulException, ParamHoraireNotFoundException;

    List<HeureServiceDTO> findAll();

    void deleteHoraireService(Integer id) throws HeureServiceNotFoundException;

    HeureServiceDTO updateHoraireService(Integer id, NewHoraireServiceDTO newHoraireServiceDTO) throws HeureServiceNotFoundException, ParamHoraireNotFoundException;
}
