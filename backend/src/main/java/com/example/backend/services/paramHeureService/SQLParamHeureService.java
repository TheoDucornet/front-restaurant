package com.example.backend.services.paramHeureService;

import com.example.backend.exceptions.ParamHoraireNotFoundException;
import com.example.backend.exceptions.ParamHoraireNulException;
import com.example.backend.model.dao.ParamHeureServiceRepository;
import com.example.backend.model.dto.in.NewHoraireDTO;
import com.example.backend.model.dto.out.HoraireDTO;
import com.example.backend.model.entities.ParamHeureServiceEntity;
import com.example.backend.model.mapper.HoraireMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SQLParamHeureService implements ParamHeureService {
    private final ParamHeureServiceRepository paramHeureServiceRepository;

    @Override
    public List<HoraireDTO> findAll() {
        return paramHeureServiceRepository.findAll().stream().map(HoraireMapper::horaireServiceToDTO).toList();
    }

    @Override
    public Optional<ParamHeureServiceEntity> addHoraire(NewHoraireDTO newHoraireDTO) throws ParamHoraireNulException {
        if (newHoraireDTO.periode().isEmpty()
                || newHoraireDTO.debut_midi() == null
                || newHoraireDTO.fin_midi() == null
                || newHoraireDTO.debut_soir() == null
                || newHoraireDTO.fin_soir() == null) {
            throw new ParamHoraireNulException();
        }
        ParamHeureServiceEntity paramHeureServiceEntity = new ParamHeureServiceEntity();

        paramHeureServiceEntity.setPeriode(newHoraireDTO.periode());
        paramHeureServiceEntity.setDebut_midi(newHoraireDTO.debut_midi());
        paramHeureServiceEntity.setFin_midi(newHoraireDTO.fin_midi());
        paramHeureServiceEntity.setDebut_soir(newHoraireDTO.debut_soir());
        paramHeureServiceEntity.setFin_soir(newHoraireDTO.fin_soir());

        paramHeureServiceRepository.save(paramHeureServiceEntity);

        return Optional.of(paramHeureServiceEntity);

    }

    @Override
    public HoraireDTO findHoraire(String periode) throws ParamHoraireNotFoundException {

        return paramHeureServiceRepository.findByPeriode(periode)
                .map(HoraireMapper::horaireServiceToDTO)
                .orElseThrow(ParamHoraireNotFoundException::new);
    }

}
