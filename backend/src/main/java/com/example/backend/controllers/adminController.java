package com.example.backend.controllers;

import com.example.backend.model.dto.out.TablesDTO;
import com.example.backend.services.tables.TablesService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class adminController {

    private final TablesService tablesService;

    @CrossOrigin(origins = "*")
    @GetMapping(Routes.GET_TABLES)
    public ResponseEntity<List<TablesDTO>> getTables() {
        return ResponseEntity.ok(tablesService.findAll());
    }

    @CrossOrigin(origins = "*")
    @PutMapping(Routes.PUT_TABLES_DISPO)
    public ResponseEntity<TablesDTO> putTableDispo(@PathVariable("numTable") final Integer numTable) {
        try {
            return ResponseEntity.ok(tablesService.updateTableDispo(numTable));
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    private String getAuthenticatedUsername() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}
