package com.example.backend.controllers;

public class Routes {
    public static final String GET_LOGIN = "/login";

    public static final String POST_RESERV = "/reserv";
    public static final String PUT_RESERV = "/reserv/{id}";
    public static final String GET_RESERV = "/reserv/{id}";
    public static final String DELETE_RESERV = "/reserv/{id}";
    public static final String GET_RESERV_HORAIRE = "/reserv/horaire/{date}";

    public static final String GET_TABLES = "/admin/salle";
    public static final String PUT_TABLES_DISPO = "/admin/table/dispo/{numTable}";
    public static final String GET_HORAIRES = "/admin/horaires";
    public static final String GET_HORAIRE = "/admin/horaires/{periode}";
    public static final String POST_HORAIRES = "/admin/horaires";
    public static final String POST_HORAIRES_SERVICE = "/admin/horaires/service";
    public static final String GET_HORAIRES_SERVICE = "/admin/horaires/service";
    public static final String PUT_HORAIRES_SERVICE = "/admin/horaires/service/{id}";
    public static final String DELETE_HORAIRES_SERVICE = "/admin/horaires/service/{id}";

}
