package com.example.backend.controllers;

import com.example.backend.model.dto.in.NewReservDTO;
import com.example.backend.services.reserv.ReservService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ReservController {
    private final ReservService reservService;

    @CrossOrigin(origins = "*")
    @PostMapping(Routes.POST_RESERV)
    public ResponseEntity<Object> postReserv(@RequestBody final NewReservDTO newReserv) {
        try {
            log.info("connection to create reservation detected");
            log.info("new reservation: " + newReserv.toString());
            return reservService.addReserv(newReserv)
                    .map(reserv -> {
                        final var uri = Routes.GET_RESERV.replace("{id}", reserv.getId().toString());
                        return ResponseEntity.created(URI.create(uri)).build();
                    }).orElse(ResponseEntity.badRequest().build());

        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

    @CrossOrigin(origins = "*")
    @GetMapping(Routes.GET_RESERV)
    public ResponseEntity<Object> getReserv(@PathVariable("id") final Integer id) {
        try {
            return ResponseEntity.ok(reservService.findById(id));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @CrossOrigin(origins = "*")
    @GetMapping(Routes.GET_RESERV_HORAIRE)
    public ResponseEntity<Object> getReservHoraire(@PathVariable("date") final LocalDate date) {
        try {
            return ResponseEntity.ok(reservService.getHoraire(date));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @CrossOrigin(origins = "*")
    @DeleteMapping(Routes.DELETE_RESERV)
    public ResponseEntity<Object> deleteReserv(@PathVariable("id") final Integer id) {
        try {
            reservService.deleterReserv(id);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @CrossOrigin(origins = "*")
    @PutMapping(Routes.PUT_RESERV)
    public ResponseEntity<Object> putReserv(@PathVariable("id") final Integer id, @RequestBody final NewReservDTO newReserv) {
        try {
            reservService.updateReserv(id, newReserv);
            return ResponseEntity.ok(URI.create(Routes.GET_RESERV.replace("{id}", id.toString())));
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

}
