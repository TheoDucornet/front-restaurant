package com.example.backend.controllers;

import com.example.backend.model.dto.in.NewHoraireDTO;
import com.example.backend.model.dto.in.NewHoraireServiceDTO;
import com.example.backend.model.dto.out.HeureServiceDTO;
import com.example.backend.model.dto.out.HoraireDTO;
import com.example.backend.model.entities.HeureServiceEntity;
import com.example.backend.services.heureService.HeureService;
import com.example.backend.services.paramHeureService.ParamHeureService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class adminHoraireController {
    private final ParamHeureService paramHeureService;
    private final HeureService heureService;

    @CrossOrigin(origins = "*")
    @GetMapping(Routes.GET_HORAIRES)
    public ResponseEntity<List<HoraireDTO>> getHoraires() {
        return ResponseEntity.ok(paramHeureService.findAll());

    }

    @CrossOrigin(origins = "*")
    @PostMapping(Routes.POST_HORAIRES)
    public ResponseEntity<Object> postHoraires(@RequestBody final NewHoraireDTO newHoraireDTO) {
        try {

            return paramHeureService.addHoraire(newHoraireDTO)
                    .map(horaire -> {
                        final var uri = Routes.GET_HORAIRE.replace("{periode}", horaire.getPeriode());
                        return ResponseEntity.created(URI.create(uri)).build();
                    }).orElse(ResponseEntity.badRequest().build());
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin(origins = "*")
    @GetMapping(Routes.GET_HORAIRE)
    public ResponseEntity<HoraireDTO> getHoraires(@PathVariable("periode") final String periode) {
        try {
            return ResponseEntity.ok(paramHeureService.findHoraire(periode));
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

    @CrossOrigin(origins = "*")
    @PostMapping(Routes.POST_HORAIRES_SERVICE)
    public ResponseEntity<Object> postHorairesService(@RequestBody final NewHoraireServiceDTO newHoraireServiceDTO) {
        try {
            heureService.addhoraireServie(newHoraireServiceDTO);
            return ResponseEntity.created(URI.create(Routes.GET_HORAIRES_SERVICE)).build();
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin(origins = "*")
    @GetMapping(Routes.GET_HORAIRES_SERVICE)
    public ResponseEntity<List<HeureServiceDTO>> getHorairesService() {
        try {
            return ResponseEntity.ok(heureService.findAll());
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin(origins = "*")
    @DeleteMapping(Routes.DELETE_HORAIRES_SERVICE)
    public ResponseEntity<Object> deleteHorairesService(@PathVariable("id") final Integer id) {
        try {
            heureService.deleteHoraireService(id);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin(origins = "*")
    @PutMapping(Routes.PUT_HORAIRES_SERVICE)
    public ResponseEntity<Object> putHorairesService(@PathVariable("id") final Integer id, @RequestBody final NewHoraireServiceDTO newHoraireServiceDTO) {
        try {
            return ResponseEntity.ok(heureService.updateHoraireService(id, newHoraireServiceDTO));
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
