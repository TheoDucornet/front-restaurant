DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS tables;
DROP TABLE IF EXISTS reserv;
DROP TABLE IF EXISTS reserv_table;
DROP TABLE IF EXISTS param_reserv;
DROP TABLE IF EXISTS param_heure_service;
DROP TABLE IF EXISTS heure_service;

CREATE TABLE users
(
    username VARCHAR(50)  NOT NULL,
    password VARCHAR(100) NOT NULL,
    enabled  BOOLEAN      NOT NULL DEFAULT TRUE,
    CONSTRAINT PK_user_username PRIMARY KEY (username)
);

CREATE TABLE tables
(
    num_table INTEGER NOT NULL,
    dispo     BOOLEAN NOT NULL DEFAULT TRUE,
    nb_places INTEGER NOT NULL,
    CONSTRAINT PK_table_num_table PRIMARY KEY (num_table)
);

CREATE TABLE reserv
(
    id           INTEGER GENERATED ALWAYS AS IDENTITY,
    date         TIMESTAMP         NOT NULL,
    email        VARCHAR(100) NOT NULL,
    tel          VARCHAR(50)  NOT NULL,
    prenom       VARCHAR(50)  NOT NULL,
    nom          VARCHAR(50)  NOT NULL,
    nb_personnes INTEGER      NOT NULL,
    CONSTRAINT PK_reserv_id PRIMARY KEY (id)
);

CREATE TABLE reserv_table
(
    id_reserv  INTEGER NOT NULL REFERENCES reserv (id),
    num_table  INTEGER NOT NULL REFERENCES tables (num_table),
    CONSTRAINT PK_reservTable_id_reserv_num_table PRIMARY KEY (id_reserv, num_table)
);

CREATE TABLE param_reserv
(
    id                INTEGER GENERATED ALWAYS AS IDENTITY,
    duree_reservation TIME NOT NULL,
    CONSTRAINT PK_param_reserv_id PRIMARY KEY (id)
);



CREATE TABLE param_heure_service
(
    periode    VARCHAR(50) NOT NULL,
    debut_midi TIME        NOT NULL,
    fin_midi   TIME        NOT NULL,
    debut_soir TIME        NOT NULL,
    fin_soir   TIME        NOT NULL,
    CONSTRAINT PK_param_heure_service_periode PRIMARY KEY (periode)
);

CREATE TABLE heure_service
(
    id         INTEGER GENERATED ALWAYS AS IDENTITY,
    date_debut DATE NOT NULL,
    date_fin   DATE NOT NULL,
    mode       VARCHAR(50) REFERENCES param_heure_service (periode)
);

INSERT INTO users (username, password, enabled)
VALUES ('admin', 'admin', TRUE);

INSERT INTO tables (num_table, dispo, nb_places)
VALUES (1, TRUE, 4),
       (2, TRUE, 4),
       (3, TRUE, 4),
       (4, TRUE, 4),
       (5, TRUE, 4),
       (6, TRUE, 4),
       (7, TRUE, 4),
       (8, TRUE, 4),
       (9, TRUE, 4),
       (10, TRUE, 4);

INSERT INTO param_reserv (duree_reservation)
VALUES ('01:00:00');

INSERT INTO param_heure_service (periode, debut_midi, fin_midi, debut_soir, fin_soir)
VALUES ('lundi', '00:00:00', '00:00:00', '00:00:00', '00:00:00'),
       ('mardi', '00:00:00', '00:00:00', '18:30:00', '22:00:00'),
       ('mercredi', '11:30:00', '14:00:00', '18:30:00', '22:00:00'),
       ('jeudi', '11:30:00', '14:00:00', '18:30:00', '22:00:00'),
       ('vendredi', '11:30:00', '14:00:00', '18:30:00', '22:00:00'),
       ('samedi', '11:30:00', '15:00:00', '18:30:00', '22:00:00'),
       ('dimanche', '11:30:00', '14:00:00', '18:30:00', '22:00:00'),
       ('ferie', '11:30:00', '14:00:00', '18:30:00', '22:00:00'),
       ('vacances', '11:30:00', '14:00:00', '18:30:00', '22:00:00'),
       ('custom', '11:30:00', '14:00:00', '18:30:00', '22:00:00');
